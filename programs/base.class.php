<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * Objects implementing app_Object_Interface are object related to a
 * specific instance of a Func_App functionality.
 *
 * These objects are generally created through one of this functionality's
 * factory methods. It is possible to retrieve this instance of Func_App by
 * using the App() method.
 */
interface app_Object_Interface
{
    /**
     * Forces the Func_App object to which this object is 'linked'.
     *
     * @param Func_App	$app
     * @return app_Object
     */
    public function setApp(Func_App $app);

    /**
     * Returns the Func_App object 'linked' to object.
     *
     * @return self
     */
    public function App();
}



class app_Object implements app_Object_Interface
{
    /**
     * @param Func_App $app
     */
    public function __construct(Func_App $app)
    {
        $this->setApp($app);
    }

    /**
     * Forces the Func_App object to which this object is 'linked'.
     *
     * @param Func_App	$app
     * @return self
     */
    public function setApp(Func_App $app)
    {
        $this->app = $app;
        return $this;
    }

    /**
     * Get APP object to use with this SET
     *
     * @return Func_App
     */
    public function App()
    {
        return $this->app;
    }
}


bab_Widgets()->includePhpClass('Widget_Item');


/**
 * The app_UiObject class is used to simulate multiple inheritance from
 * both a app_Object and a Widget_Item
 *
 * Typically to simulate inheritance from a Widget_VBoxLayout for example,
 * the constructor of the class will be defined as follow:
 * <pre>
 * 	public function __construct($app, $id = null)
 *	{
 *		parent::__construct($app);
 *		// We simulate inheritance from Widget_VBoxLayout.
 *		$this->setInheritedItem(bab_Functionality::get('Widgets')->VBoxLayout($id));
 *	}
 * </pre>
 */
class app_UiObject extends app_Object implements Widget_Displayable_Interface
{
    /**
     * @var Widget_Item $item
     */
    protected $item = null;
    
    
    /**
     * @param Func_App $app
     */
    public function __construct(Func_App $app)
    {
        parent::__construct($app);
    }
    
    /**
     * Sets the item from which we will 'inherit'.
     *
     * @param Widget_Item	$item
     * @return app_UiObject
     */
    public function setInheritedItem($item)
    {
        $this->item = $item;
        return $this;
    }
    
    /**
     * @ignore
     */
    public function __call($name, $arguments)
    {
        // We delegate all undefined methods to the $item object.
        if (isset($this->item)) {
            $returnedValue = call_user_func_array(array($this->item, $name), $arguments);
            if ($returnedValue === $this->item) {
                $returnedValue = $this;
            }
            return $returnedValue;
        } else {
            trigger_error('the method '.$name.' does not exists on '.get_class($this).' and there is no widget defined with the setInheritedItem method');
        }
    }
    
    /**
     * @param Widget_Canvas	$canvas
     * @ignore
     */
    public function display(Widget_Canvas $canvas)
    {
        return $this->item->display($canvas);
    }
}
