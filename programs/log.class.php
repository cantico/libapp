<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */

$App = app_App();
$App->includeRecordSet();


/**
 * @property ORM_StringField    $objectClass
 * @property ORM_IntField       $objectId
 * @property ORM_DateTimeField  $modifiedOn
 * @property ORM_UserField      $modifiedBy
 * @property ORM_BoolField      $noTrace
 * @property ORM_TextField      $data
 *
 * @method app_Log                  get()
 * @method app_Log                  request()
 * @method app_Log[]|\ORM_Iterator  select()
 * @method app_Log                  newRecord()
 * @method Func_App App()
 */
class app_LogSet extends app_RecordSet
{
    public function __construct(Func_App $App = null)
    {
        parent::__construct($App);

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('objectClass')
                ->setDescription('Created by'),
            ORM_IntField('objectId')
                ->setDescription('Modified by'),
            ORM_DateTimeField('modifiedOn')
                ->setDescription('Modified on'),
            ORM_UserField('modifiedBy')
                ->setDescription('Modified by'),
            ORM_BoolField('noTrace')
                ->setDescription('Not traced'),
            ORM_TextField('data')
                ->setDescription('Serialized object data')
        );
    }

    /**
     * Serialize of app_Record in order to store it in the log.
     *
     * @return array
     */
    public function serialize(app_Record $record)
    {
        $values = $record->getValues();
        foreach ($values as $key => $value) {
            if ($value instanceof ORM_Set) {
                $values[$key] = $value->id;
            }
        }

        return serialize($values);
    }



    /**
     * Match log records for the specified app_Record.
     *
     * @param app_Record $record
     * @return ORM_Criteria
     */
    public function hasObject(app_Record $record)
    {
        return $this->objectClass->is(get_class($record))->_AND_($this->objectId->is($record->id));
    }
}



/**
 * @property string    $objectClass
 * @property int       $objectId
 * @property string    $modifiedOn
 * @property int       $modifiedBy
 * @property bool      $noTrace
 * @property string    $data
 *
 * @method app_LogSet getParent()
 * @method Func_App App()
 */
class app_Log extends app_Record
{
}
