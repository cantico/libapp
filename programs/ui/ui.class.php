<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * The app_Ui class
 */
class app_Ui extends app_Object
{
    /**
     * Includes app Ui helper functions definitions.
     */
    public function includeBase()
    {
        require_once APP_UI_PATH . 'base.ui.php';
        require_once APP_UI_PATH . 'ui.helpers.php';
    }

    public function includePage()
    {
        require_once APP_UI_PATH . 'page.class.php';
    }

    /**
     * Return App page widget
     *
     * @return app_Page
     */
    public function Page($id = null, Widget_Layout $layout = null)
    {
        $App = $this->App();

        $this->includePage();
        $page = new app_Page($id, $layout);
        $page->setApp($App);
        $page->setNumberFormat($App->numberFormat(1.1111111));
        $page->addMessageInfo();

        return $page;
    }

    /**
     * @return Widget_Layout
     */
    public function Toolbar()
    {
        $W = bab_Widgets();

        return $W->FlowItems()->addClass(Func_Icons::ICON_LEFT_16, 'widget-toolbar');
    }


    /**
     * @return app_ExportSelectEditor
     */
    public function ExportSelectEditor($id, app_TableModelView $tableview, $filter = null)
    {
        require_once dirname(__FILE__) . '/exportselect.ui.php';
        return new app_ExportSelectEditor($this->App(), $id, $tableview, $filter);
    }
}