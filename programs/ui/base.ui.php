<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

bab_Widgets()->includePhpClass('widget_Form');
bab_Widgets()->includePhpClass('widget_Frame');
bab_Widgets()->includePhpClass('widget_InputWidget');
bab_Widgets()->includePhpClass('widget_TableModelView');

require_once dirname(__FILE__) . '/ui.helpers.php';

/**
 * @return Widget_Form
 *
 *
 * @method self setReadOnly()
 * @method boolean isReadOnly()
 * @method boolean getReadOnly()
 * @method self colon()
 * @method boolean isColon()
 * @method self setHiddenValue()
 * @method string getHiddenValue()
 * @method array getHiddenFields()
 * @method self setHiddenValues()
 * @method Widget_InputWidget[] getFields()
 * @method mixed getValue()
 * @method self setValue()
 * @method self setValues()
 * @method array getValues()
 * @method self setAnchor()
 * @method self setSelfPageHiddenFields()
 * @method boolean testMandatory()
 * @method self checkUnsaved()
 * @method self setValidateAction()
 * @method array getClasses()
 * @method array getFullName()
 * @method string display()
 */
class app_Editor extends app_UiObject
{
    /**
     * @var app_Record
     */
    protected $record;

    protected $ctrl;

    protected $saveAction;
    protected $saveLabel;

    protected $cancelAction;
    protected $cancelLabel;

    /* @var $widgets Func_Widgets */
    protected $widgets;

    protected $buttonsLayout;
    protected $innerLayout;

    protected $failedAction = null;
    protected $successAction = null;

    /**
     * @var Widget_Section[] $sections
     */
    protected $sections = array();


    public $isAjax = false;

    /**
     *
     * @var array
     */
    private $tmp_values;


    /**
     * @param	Func_App		$app
     * @param 	Widget_Layout 	$layout		The layout that will manage how widgets are displayed in this form.
     * @param 	string 			$id			The item unique id.
     */
    public function __construct(Func_App $app, $id = null, Widget_Layout $layout = null)
    {
        parent::__construct($app);
         // We simulate inheritance from Widget_VBoxLayout.
        $W = $this->widgets = bab_Widgets();


        $this->innerLayout = $layout;
        $this->buttonsLayout = $this->buttonsLayout();

        if (!isset($this->innerLayout)) {
            $this->innerLayout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        }

        $this->innerLayout->addClass('app-loading-box'); // javascript can add the app-loading-on class to this container to show ajax loading progress in the form

        $layout = $W->VBoxItems(
            $this->innerLayout,
            $this->buttonsLayout
        );
        $layout->setVerticalSpacing(2, 'em');
//		parent::__construct($id, $layout);

        // We simulate inheritance from Widget_Form.
        $this->setInheritedItem($W->Form($id, $layout));
        if (isset($this->tmp_values))
        {
            $this->setValues($this->tmp_values[0], $this->tmp_values[1]);
        }

        $this->setFormStyles();
        $this->prependFields();

    }


    /**
     * @return Widget_Layout
     */
    protected function buttonsLayout()
    {
        $W = bab_Widgets();
        return $W->HBoxLayout()->setHorizontalSpacing(1, 'em');
    }



    protected function setFormStyles()
    {
        $this->addClass('app-editor');
    }


    /**
     * Set record and values to editor
     * @see app_Editor::getRecord()
     *
     * @param app_Record $record
     *
     * @return self
     */
    public function setRecord(app_Record $record)
    {
        $this->record = $record;

        if ($name = $this->getName()) {
            $values = $record->getFormOutputValues();

            $this->setValues($values, array($name));
        }

        return $this;
    }

    /**
     * @return app_Record
     */
    protected function getRecord()
    {
        return $this->record;
    }


    /**
     * @return app_RecordSet
     */
    protected function getRecordSet()
    {
        if ($this->record) {
            return $this->record->getParentSet();
        }
        return null;
    }


    public function setValues($row, $namePathBase = array())
    {
        if (isset($this->item)) {
            return $this->item->setValues($row, $namePathBase);
        } else {
            $this->tmp_values = array($row, $namePathBase);
            return true;
        }
    }

    /**
     *
     * @return app_Editor
     */
    public function setController($ctrl)
    {
        $this->ctrl = $ctrl;
        return $this;
    }


    /**
     *
     * @return app_Editor
     */
    public function setSaveAction($saveAction, $saveLabel = null)
    {
        $this->saveAction = $saveAction;
        $this->saveLabel = $saveLabel;
        return $this;
    }

    public function setSuccessAction($successAction)
    {
        $this->successAction = $successAction;
        return $this;
    }

    public function setFailedAction($failedAction)
    {
        $this->failedAction = $failedAction;
        return $this;
    }

    /**
     *
     * @return app_Editor
     */
    public function setCancelAction($cancelAction, $cancelLabel = null)
    {
        $this->cancelAction = $cancelAction;
        $this->cancelLabel = $cancelLabel;
        return $this;
    }




    /**
     * Adds a button to button box of the form.
     *
     * @return app_Editor
     */
    public function addButton(Widget_Item $button, Widget_Action $action = null)
    {
        $this->buttonsLayout->addItem($button);
        if (isset($action)) {
            $button->setAction($action);
        }

        return $this;
    }


    /**
     * Adds an item to the top part of the form.
     *
     * @return app_Editor
     */
    public function addItem(Widget_Displayable_Interface $item = null, $position = null)
    {
        $this->innerLayout->addItem($item, $position);

        return $this;
    }

    /**
     *
     *
     */
    protected function appendButtons()
    {
        $W = $this->widgets;
        $App = $this->App();

        if (isset($this->saveAction)) {
            $saveLabel = isset($this->saveLabel) ? $this->saveLabel : $App->translate('Save');
            $submitButton = $W->SubmitButton();
            $submitButton->validate(true)
                ->setAction($this->saveAction)
                ->setFailedAction($this->failedAction)
                ->setSuccessAction($this->successAction)
                ->setLabel($saveLabel);
            if ($this->isAjax) {
                $submitButton->setAjaxAction();
            }
            $this->addButton($submitButton);
        }

        if (isset($this->cancelAction)) {
            $cancelLabel = isset($this->cancelLabel) ? $this->cancelLabel : $App->translate('Cancel');
            $this->addButton(
                $W->SubmitButton(/*'cancel'*/)
                     ->addClass('widget-close-dialog')
                    ->setAction($this->cancelAction)
                      ->setLabel($cancelLabel)
            );
        }

    }

    /**
     * Fields that will appear at the beginning of the form.
     *
     */
    protected function prependFields()
    {

    }


    /**
     * Fields that will appear at the end of the form.
     *
     */
    protected function appendFields()
    {

    }


    /**
     * Adds an item with a label and a description to the form.
     *
     * @param string                       $labelText
     * @param Widget_Displayable_Interface $item
     * @param string                       $fieldName
     * @param string                       $description
     * @param string						$suffix		 suffix for input field, example : unit
     *
     * @return Widget_LabelledWidget
     */
    public function labelledField($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null)
    {
        $W = $this->widgets;
        return $W->LabelledWidget($labelText, $item, $fieldName, $description, $suffix);
    }


    /**
     * Creates section in the editor.
     * If a section with the same header text (title) was already
     * created it is replaced by an empty section.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function createSection($headerText, $layout = null)
    {
        $W = bab_Widgets();
        if (!isset($layout)) {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        }
        $this->sections[$headerText] = $W->Section(
            $headerText,
            $layout
        )->setFoldable(true);

        return $this->sections[$headerText];
    }

    /**
     * Retrieves a section in the editor.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function getSection($headerText)
    {
        if (!isset($this->sections[$headerText])) {
            return null;
        }
        return $this->sections[$headerText];
    }


    /**
     * Returns the list of sections created via createSection().
     *
     * @return Widget_Section[]
     */
    public function getSections()
    {
        return $this->sections;
    }


    /**
     *
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->appendFields();
        $this->appendButtons();

//         if (false === $this->getReadOnly())
//         {
//             if ($bItem = app_BreadCrumbs::getLastest())
//             {
//                 $this->setHiddenValue('_ctrl_previous', $bItem->uid);
//             }
//         }


        return parent::display($canvas);
    }
}


class app_Toolbar extends Widget_Frame
{
    public $local = false;

    public function __construct($id = null, Widget_Layout $layout = null)
    {
        if (!isset($layout)) {
            $W = bab_Widgets();
            $layout = $W->FlowLayout()->setHorizontalSpacing(1, 'em');
            $layout->setVerticalAlign('top');
        }

        parent::__construct($id, $layout);
    }

    /**
     *
     * @param string		 	$labelText
     * @param string			$iconName
     * @param Widget_Action		$action
     * @param string			$id
     * @return app_Toolbar
     */
    public function addButton($labelText = null, $iconName = null, $action = null, $id = null)
    {
        $W = bab_Widgets();
        $button = $W->Link($labelText, $action, $id);
        if (isset($iconName)) {
            $button->addClass('icon', $iconName);
        }

        $this->addItem($button);

        return $this;
    }


    public function addInstantForm(Widget_Displayable_Interface $form, $labelText = null, $iconName = null, $action = null, $id = null)
    {
        $W = bab_Widgets();
        if (isset($iconName)) {
            $content = $W->Icon($labelText, $iconName);
        } else {
            $content = $labelText;
        }
        $button = $W->Link($content, $action, $id);

        $this->addItem(
            $W->VBoxItems(
                $button->addClass('widget-instant-button'),
                $form->addClass('widget-instant-form')
            )->addClass('widget-instant-container')
        );

        if ($form->getTitle() === null) {
            $form->setTitle($labelText);
        }


        return $this;

    }


    public function display(Widget_Canvas $canvas)
    {
        if (!$this->local) {
            $this->addClass('widget-toolbar');
        } else {
            $this->addClass('app-toolbar');
        }
        $this->addClass(Func_Icons::ICON_LEFT_16);

        return parent::display($canvas);
    }

}




class app_RecordEditor extends app_Editor
{

    /**
     * @var app_RecordSet
     */
    protected $recordSet = null;

    /**
     * Creates section in the editor.
     * If a section with the same header text (title) was already
     * created it is replaced by an empty section.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function addSection($id, $headerText, $layout = null)
    {
        $W = bab_Widgets();
        if (!isset($layout)) {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        }
        $this->sections[$id] = $W->Section(
            $headerText,
            $layout
        )->setFoldable(true);

        return $this->sections[$id];
    }


    /**
     * Retrieves a section in the editor.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function getSection($id)
    {
        if (!isset($this->sections[$id])) {
            return null;
        }
        return $this->sections[$id];
    }


    /**
     * @param string $textLabel
     * @param mixed $value
     * @return Widget_Layout
     */
    function labelledWidget($textLabel, $value, app_CustomSection $section = null)
    {
        $W = bab_Widgets();

        if ($value instanceof Widget_Displayable_Interface) {
            $widget = $value;
        } else {
            $widget = $W->Label($value);
        }

        if (isset($section)) {
            if ($textLabel === '__') {
                $fieldLayout = app_CustomSection::FIELDS_LAYOUT_NO_LABEL;
            } else {
                $fieldLayout = $section->fieldsLayout;
            }
            switch ($fieldLayout) {
                case app_CustomSection::FIELDS_LAYOUT_HORIZONTAL_LABEL:
                    return $W->FlowItems(
                        $W->Label($textLabel)->addClass('app-horizontal-display-label', 'widget-strong')
                            ->setSizePolicy('widget-25pc'),
                        $widget
                    )->setHorizontalSpacing(1, 'ex')
                    ->setVerticalAlign('top');

                case app_CustomSection::FIELDS_LAYOUT_WIDE_HORIZONTAL_LABEL:
                    return $W->FlowItems(
                        $W->Label($textLabel)->addClass('app-horizontal-display-label', 'widget-strong')
                            ->setSizePolicy('widget-50pc'),
                        $widget
                    )->setHorizontalSpacing(1, 'ex')
                    ->setVerticalAlign('top');

                case app_CustomSection::FIELDS_LAYOUT_NO_LABEL:
                    return $widget;

                case app_CustomSection::FIELDS_LAYOUT_VERTICAL_LABEL:
                default:
                    return $W->LabelledWidget($textLabel, $widget);
            }
        }
    }


    function labelledWidgetOptional($textLabel, $displayedValue, $value = null, app_CustomSection $section = null)
    {
        if (!isset($value)) {
            $value = $displayedValue;
        }
        if (!isset($value) || is_numeric($value) && $value == 0 || is_string($value) && trim($value) == '' || ($value instanceof Widget_Layout && count($value->getItems()) <= 0)) {
            return null;
        }
        return $this->labelledWidget($textLabel, $displayedValue, $section);
    }



    protected function fieldOutput($field, $record, $fieldName)
    {
        if ($field instanceof ORM_CurrencyField) {
            $App = $this->App();
            $value = $App->shortFormatWithUnit($this->record->$fieldName, $App->translate('_euro_'));
        } else {
            $value = $field->output($this->record->$fieldName);
        }
        if (is_array($value)) {
            $value = implode(', ', $value);
        }
        return $value;
    }


    protected function getValueItem($customSection, $displayField, $label, $value)
    {
//         if ($value instanceof Widget_Displayable_Interface) {
//             return $value;
//         }
//         $W = bab_Widgets();
//         $parameters = $displayField['parameters'];
//         if ($parameters['type'] === 'title1') {
//             $item = $W->Title($value, 1);
//         } elseif ($parameters['type'] === 'title2') {
//             $item = $W->Title($value, 2);
//         } elseif ($parameters['type'] === 'title3') {
//             $item = $W->Title($value, 3);
//         } else {
//             $item = $W->Label($value);
//         }
        return $this->labelledWidget(
            $label,
//            $item,
            $value,
            $customSection
        );
    }


    protected function addSections($view)
    {
        $App = $this->App();
        $W = bab_Widgets();


        $this->addItem($W->Hidden()->setName('id'));

        $recordClassName = $this->record->getClassName();

        $customSectionSet = $App->CustomSectionSet();
        $customSections = $customSectionSet->select(
            $customSectionSet->object->is($recordClassName)->_AND_(
                $customSectionSet->view->is($view)
            )
        );
        $customSections->orderAsc($customSectionSet->rank);

        $currentColumn = 0;
        $row = $W->Items()->setSizePolicy('row');
        foreach ($customSections as $customSection) {

            if (isset($this->record) && !$customSection->isVisibleForRecord($this->record)) {
                continue;
            }

            list(, , $nbCol) = explode('-', $customSection->sizePolicy);

            if ($currentColumn + $nbCol > 12) {
                $this->addItem($row);
                $row = $W->Items()->setSizePolicy('row');
                $currentColumn = 0;
            }
            $currentColumn += $nbCol;

            $section = $this->getSection($customSection->id);
            if (!isset($section)) {
                $section = $this->addSection($customSection->id, $customSection->name);
                $section->addClass($customSection->classname);
                $section->setSizePolicy($customSection->sizePolicy);

                $section->setFoldable($customSection->foldable, $customSection->folded);
                $row->addItem($section);
            }

            $displayFields = $customSection->getFields();

            foreach ($displayFields as $displayField) {
                $widget = null;
                $item = null;
                $displayFieldName = $displayField['fieldname'];
                $parameters = $displayField['parameters'];
                $classname = isset($parameters['classname']) ? $parameters['classname'] : '';
                $label = isset($parameters['label']) && $parameters['label'] !== '__' ? $parameters['label'] : '';
                $displayFieldMethod = '_' . $displayFieldName;

                if (method_exists($this, $displayFieldMethod)) {
                    $widget = $this->$displayFieldMethod($customSection);
                    $item = $widget;
                } elseif ($this->recordSet->fieldExist($displayFieldName)) {
                    $field = $this->recordSet->getField($displayFieldName);
                    if ($label === '') {
                        $label = $field->getDescription();
                        if (substr($displayFieldName, 0, 1) !== '_') {
                            $label = $App->translate($label);
                        }
                    }
                    $widget = $field->getWidget();
                    if ($widget instanceof Widget_TextEdit || $widget instanceof Widget_Select) {
                        $widget->addClass('widget-100pc');
                    }
                    $item = $this->getValueItem($customSection, $displayField, $label, $widget);
                    //$value = $this->fieldOutput($field, $this->record, $displayFieldName);
                }

//                 if (isset($widget)) {
//                     $item = $this->getValueItem($customSection, $displayField, $label, $widget);
                    if ($item) {
                        $item->addClass($classname);
                        $section->addItem($item);
                    }
//                 }
            }
        }

        if ($currentColumn + $nbCol> 0) {
            $this->addItem($row);
        }
    }
}



/**
 * A record frame is a frame displaying information about a app record.
 *
 * @since 1.0.40
 */
class app_RecordFrame extends app_UiObject // extends Widget_Frame
{

    /**
     * @var app_Record
     */
    protected $record = null;

    public function __construct(Func_App $app, app_Record $record, $id = null, Widget_Layout $layout = null)
    {
        parent::__construct($app);

        $W = bab_Widgets();
        $this->setInheritedItem($W->Frame($id, $layout));
    }


    /**
     * Creates section in the editor.
     * If a section with the same header text (title) was already
     * created it is replaced by an empty section.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function addSection($id, $headerText, $layout = null)
    {
        $W = bab_Widgets();
        if (!isset($layout)) {
            $layout = $W->VBoxLayout()->setVerticalSpacing(0.6, 'em');
        }
        $this->sections[$id] = $W->Section(
            $headerText,
            $layout
        )->setFoldable(true);

        return $this->sections[$id];
    }


    /**
     * Retrieves a section in the editor.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function getSection($id)
    {
        if (!isset($this->sections[$id])) {
            return null;
        }
        return $this->sections[$id];
    }


    /**
     * @param string $textLabel
     * @param mixed $value
     * @return Widget_Layout
     */
    function labelledWidget($textLabel, $value, app_CustomSection $section = null)
    {
        $W = bab_Widgets();

        if ($value instanceof Widget_Displayable_Interface) {
            $widget = $value;
        } else {
            $widget = $W->Label($value);
        }

        if (isset($section)) {
            if ($textLabel === '__') {
                $fieldLayout = app_CustomSection::FIELDS_LAYOUT_NO_LABEL;
            } else {
                $fieldLayout = $section->fieldsLayout;
            }
            switch ($fieldLayout) {
                case app_CustomSection::FIELDS_LAYOUT_HORIZONTAL_LABEL:
                    return $W->FlowItems(
                    $W->Label($textLabel)->addClass('app-horizontal-display-label', 'widget-strong')
                        ->setSizePolicy('widget-25pc'),
                    $widget->setSizePolicy('widget-75pc')
                )->setHorizontalSpacing(1, 'ex')
                ->setVerticalAlign('top');

                case app_CustomSection::FIELDS_LAYOUT_WIDE_HORIZONTAL_LABEL:
                    return $W->FlowItems(
                    $W->Label($textLabel)->addClass('app-horizontal-display-label', 'widget-strong')
                        ->setSizePolicy('widget-50pc'),
                    $widget->setSizePolicy('widget-50pc')
                )->setHorizontalSpacing(1, 'ex')
                ->setVerticalAlign('top');

                case app_CustomSection::FIELDS_LAYOUT_VERY_WIDE_HORIZONTAL_LABEL:
                    return $W->FlowItems(
                    $W->Label($textLabel)->addClass('app-horizontal-display-label', 'widget-strong')
                        ->setSizePolicy('widget-75pc'),
                    $widget->setSizePolicy('widget-25pc')
                )->setHorizontalSpacing(1, 'ex')
                ->setVerticalAlign('top');


                case app_CustomSection::FIELDS_LAYOUT_NO_LABEL:
                    return $widget;

                case app_CustomSection::FIELDS_LAYOUT_VERTICAL_LABEL:
                default:
                    return $W->LabelledWidget($textLabel, $widget);
            }
        }
    }


    function labelledWidgetOptional($textLabel, $displayedValue, $value = null, app_CustomSection $section = null)
    {
        if (!isset($value)) {
            $value = $displayedValue;
        }
        if (!isset($value) || is_numeric($value) && $value == 0 || is_string($value) && trim($value) == '' || ($value instanceof Widget_Layout && count($value->getItems()) <= 0)) {
            return null;
        }
        $labelledWidget = $this->labelledWidget($textLabel, $displayedValue, $section);
        return $labelledWidget;
    }



    protected function fieldOutput($field, $record, $fieldName)
    {
        if ($field instanceof ORM_CurrencyField) {
            $App = $this->App();
            $value = $App->shortFormatWithUnit($this->record->$fieldName, $App->translate('_euro_'));
        } else {
            $value = $field->output($this->record->$fieldName);
        }
        if (is_array($value)) {
            $value = implode(', ', $value);
        }
        return $value;
    }


    protected function getValueItem($customSection, $displayField, $label, $value)
    {
        if ($value instanceof Widget_Displayable_Interface) {
            return $value;
        }
        $W = bab_Widgets();
        $parameters = $displayField['parameters'];
        if (isset($parameters['type'])) {
            if ($parameters['type'] === 'title1') {
                $item = $W->Title($value, 1);
            } elseif ($parameters['type'] === 'title2') {
                $item = $W->Title($value, 2);
            } elseif ($parameters['type'] === 'title3') {
                $item = $W->Title($value, 3);
            } else {
                $item = $W->Label($value);
            }
        } else {
            $item = $W->Label($value);
        }
        $labelledWidgetOptional = $this->labelledWidget( //Optional(
            $label,
            $item,
//            $value,
            $customSection
        );

        if (isset($labelledWidgetOptional)) {
            $labelledWidgetOptional->addClass($displayField['fieldname']);
        }

        return $labelledWidgetOptional;
    }


    protected function addSections($view)
    {
        $App = $this->App();
        $W = bab_Widgets();

        $recordClassName = $this->record->getClassName();

        $customSectionSet = $App->CustomSectionSet();
        $customSections = $customSectionSet->select(
            $customSectionSet->object->is($recordClassName)->_AND_(
                $customSectionSet->view->is($view)
            )
        );
        $customSections->orderAsc($customSectionSet->rank);

        $currentColumn = 0;
        $row = $W->Items()->setSizePolicy('row');
        foreach ($customSections as $customSection) {

            list(, , $nbCol) = explode('-', $customSection->sizePolicy);

            if ($currentColumn + $nbCol > 12) {
                $this->addItem($row);
                $row = $W->Items()->setSizePolicy('row');
                $currentColumn = 0;
            }
            $currentColumn += $nbCol;

            $section = $this->getSection($customSection->id);
            if (!isset($section)) {
                $section = $this->addSection($customSection->id, $customSection->name);
                $section->addClass($customSection->classname);
                $section->setSizePolicy($customSection->sizePolicy);

                $section->setFoldable($customSection->foldable, $customSection->folded);
                $row->addItem($section);
            }

            $displayFields = $customSection->getFields();

            foreach ($displayFields as $displayField) {
                $value = null;
                $displayFieldName = $displayField['fieldname'];
                $parameters = $displayField['parameters'];
                $classname = isset($parameters['classname']) ? $parameters['classname'] : '';
                $label = isset($parameters['label']) ? $parameters['label'] : '';
                $displayFieldMethod = '_' . $displayFieldName;

                if (method_exists($this, $displayFieldMethod)) {
                    $value = $this->$displayFieldMethod($customSection);
                } elseif ($this->recordSet->fieldExist($displayFieldName)) {
                    $field = $this->recordSet->getField($displayFieldName);
                    if ($label === '') {
                        $label = $field->getDescription();
                        if (substr($displayFieldName, 0, 1) !== '_') {
                            $label = $App->translate($label);
                        }
                    }
                    $value = $this->fieldOutput($field, $this->record, $displayFieldName);
                }

                if (isset($value)) {
                    $item = $this->getValueItem($customSection, $displayField, $label, $value);
                    if ($item) {
                        $item->addClass($classname);
                        $section->addItem($item);
                    }
                }
            }
        }

        if ($currentColumn + $nbCol> 0) {
            $this->addItem($row);
        }
    }
}

/**
 *
 * @param Widget_FilePickerItem $file
 * @param int                   $width
 * @param int                   $height
 * @return Widget_Icon
 */
function app_fileAttachementIcon($file, $width, $height)
{
    $App = app_App();
    $W = bab_Widgets();
    $T = @bab_functionality::get('Thumbnailer');
    $F = @bab_functionality::get('FileInfos');

    $fileSize = filesize($file->getFilePath()->tostring());
    if ($fileSize > 1024 * 1024) {
        $fileSizeText = round($fileSize / (1024 * 1024), 1) . ' ' . $App->translate('MB');
    } elseif ($fileSize > 1024) {
        $fileSizeText = round($fileSize / 1024) . ' ' . $App->translate('KB');
    } else {
        $fileSizeText = $fileSize . ' ' . $App->translate('Bytes');
    }

    $fileIcon = $W->FileIcon($file->toString() . "\n" . $fileSizeText, $file->getFilePath())
        ->setThumbnailSize($width, $height);

    return $fileIcon;
}










/**
 * Table model view with App() method
 *
 *
 */
class app_TableModelView extends widget_TableModelView
{

    /**
     * Filter form reset button
     */
    protected $reset = null;


    /**
     * @param Func_App $app
     * @param string $id
     */
    public function __construct(Func_App $app = null, $id = null)
    {
        parent::__construct(null, $id);
        $this->setApp($app);
    }

    /**
     * Forces the Func_App object to which this object is 'linked'.
     *
     * @param Func_App	$app
     * @return app_RecordSet
     */
    public function setApp(Func_App $app = null)
    {
        $this->app = $app;
        return $this;
    }

    /**
     * Get App object to use with this SET
     *
     * @return Func_App
     */
    public function App()
    {
        if (!isset($this->app)) {
            // If the app object was not specified (through the setApp() method)
            // we try to select one according to the classname prefix.
            list($prefix) = explode('_', get_class($this));
            $functionalityName = ucwords($prefix);
            $this->app = @bab_functionality::get('App/' . $functionalityName);
            if (!$this->app) {
                $this->app = @bab_functionality::get('App');
            }
        }
        return $this->app;
    }



	protected function addCustomFields(app_RecordSet $recordSet)
	{
		$customFields = $recordSet->getCustomFields();

        foreach ($customFields as $customField) {
            $fieldname = $customField->fieldname;
            $this->addColumn(
                widget_TableModelViewColumn($recordSet->$fieldname, $customField->name)
                  ->setSortable(true)
                  ->setExportable(true)
                  ->setSearchable($customField->searchable)
				  ->setVisible($customField->visible)
            );
        }
	}


    /**
     * Get a generic filter panel
     * Use setPageLength to define the default number of items per page
     *
     * @param	array	$filter		Filter values
     * @param	string	$name		filter form name
     * @param	string	$anchor		anchor in destination page of filter, the can be a Widget_Tab id
     *
     * @return Widget_Filter
     */
    public function filterPanel($filter = null, $name = null)
    {
        $W = bab_Widgets();

        $filterPanel = $W->Filter();
        $filterPanel->setLayout($W->VBoxLayout());
        if (isset($name)) {
            $filterPanel->setName($name);
        }

        $pageLength = $this->getPageLength();
        if (null === $pageLength) {
            $pageLength = 15;
        }

        $pageNumber = $this->getCurrentPage();
        if (null === $pageNumber) {
            $pageNumber = 0;
        }

        $this->setPageLength(isset($filter['pageSize']) ? $filter['pageSize'] : $pageLength);
        $this->setCurrentPage(isset($filter['pageNumber']) ? $filter['pageNumber'] : $pageNumber);

        if (isset($name)) {
            $this->sortParameterName = $name . '[filter][sort]';
        } else {
            $this->sortParameterName = 'filter[sort]';
        }

        if (isset($filter['sort'])) {
            $this->setSortField($filter['sort']);
        } elseif (!isset($this->sortField)) {

            if (method_exists($this, 'getDefaultSortField'))
            {
                $this->setSortField($this->getDefaultSortField());
            } else {
                $columns = $this->getVisibleColumns();
                list($sortField) = each($columns);
                $this->setSortField($sortField);
            }
        }

        $form = $this->getFilterForm();

        if (isset($filter)) {
            if (isset($name)) {
                $path = array($name, 'filter');
            } else {
                $path = array('filter');
            }
            $form->setValues($filter, $path);
        }


        $filterPanel->setFilter($form);
        $filterPanel->setFiltered($this);

        return $filterPanel;
    }


    /**
     * Add the filter fields to the filter form
     * @param Widget_Form $form
     *
     */
    protected function handleAdvancedFilterFields(Widget_Item $form)
    {

    }




    protected function isFilterFieldSpecified($filter, $fieldPath)
    {
        if (isset($filter[$fieldPath])) {
            $value = $filter[$fieldPath];
            if (is_array($value)) {
                foreach ($value as $val) {
                    $val = trim($val);
                    if (!empty($val)) {
                        return true;
                    }
                }
                return false;
            }

            if (trim($value) !== '') {
                return true;
            }
        }

        return false;
    }



    /**
     * Handle label and input widget merge in one item before adding to the filter form
     * default is a vertical box layout
     *
     * @param Widget_Label                  $label
     * @param Widget_Displayable_Interface  $input
     * @return Widget_Item
     */
    protected function handleFilterLabel(Widget_Label $label, Widget_Displayable_Interface $input)
    {
        $W = bab_Widgets();
        if ($input instanceof Widget_CheckBox) {
            return $W->HBoxItems(
                $input,
                $label
            )->setVerticalAlign('middle')
            ->setHorizontalSpacing(1, 'ex');
        }
        return $W->VBoxItems(
            $label,
            $input
        );
    }

    protected function getSearchItem()
    {
        $W = bab_Widgets();

        return $W->LineEdit()->setName('search')->addClass('widget-100pc');
    }

    /**
     * Get an advanced form filter
     * @see Widget_Filter
     *
     * @param   string          $id
     * @param   array           $filter
     *
     * @return Widget_Form
     */
    public function getAdvancedFilterForm($id = null, $filter = null)
    {
        $App = $this->App();

        $W = bab_Widgets();

        $formItem = $this->getSearchItem();

        $activeFiltersFrame = $W->Frame(
            null,
            $W->FlowItems(
                $W->VBoxItems(
                    $W->Label($App->translate('Search'))->setAssociatedWidget($formItem),
                    $formItem
                )->setSizePolicy('col-lg-2 col-md-3 col-sm-6 col-xs-12')
            )
            ->setHorizontalSpacing(1, 'em')
            ->setVerticalSpacing(1, 'em')
            ->setVerticalAlign('bottom')
        );

        $advancedFiltersFrame = $W->Section(
            $App->translate('Advanced filters'),
            $W->FlowLayout()
                ->setHorizontalSpacing(1, 'em')
                ->setVerticalSpacing(1, 'em')
                ->setVerticalAlign('bottom'),
            7
        )->setFoldable(true, true)

        ->setSizePolicy('row');


        $columns = $this->getVisibleColumns();

        foreach ($columns as $fieldName => $column) {
            $field = $column->getField();
            if (! $column->isSearchable()) {
                continue;
            }

            if (! ($field instanceof ORM_Field)) {
                $field = null;
            }

            $label = $this->handleFilterLabelWidget($fieldName, $field);
            $input = $this->handleFilterInputWidget($fieldName, $field);


            if (isset($input) && isset($label)) {

                $input->setName($fieldName);
                $label->setAssociatedWidget($input);

                $input->addClass('widget-100pc');

                $formItem = $this->handleFilterLabel($label, $input);
                $formItem->setSizePolicy('col-lg-2 col-md-3 col-sm-6 col-xs-12');
                $formItem->addClass('field_' . $fieldName);

                $mainSearch = (method_exists($column, 'isMainSearch') && $column->isMainSearch());

                if ($mainSearch || $this->isFilterFieldSpecified($filter, $column->getFieldPath())) {
                    $activeFiltersFrame->addItem($formItem);
                } else {
                    $advancedFiltersFrame->addItem($formItem);
                }
            }
        }


        if (! $this->submit) {
            $this->submit = $W->SubmitButton();
            $this->submit->setLabel(widget_translate('Filter'));
        }


        $form = $W->Form($id);
        $form->setReadOnly(true);
        $form->setName('filter');
        $form->colon();

        $form->setLayout(
            $W->VBoxItems(
                $activeFiltersFrame
                    ->setSizePolicy('row'),
                $buttonsBox = $W->FlowItems(
                    $advancedFiltersFrame,
                    $this->submit,
                    $this->reset
                )->setHorizontalSpacing(2, 'em')
                ->setSizePolicy('row')
            )->setVerticalSpacing(1, 'em')
            ->setVerticalAlign('bottom')
        );

        $form->setHiddenValue('tg', $App->controllerTg);
        $form->setAnchor($this->getAnchor());



        if ($this->isColumnSelectionAllowed()) {
            $columnSelectionMenu = $this->columnSelectionMenu($this->columns);
            $buttonsBox->addItem($columnSelectionMenu);
        }

        return $form;
    }


    /**
     * Get a advanced filter panel
     * Use setPageLength to define the default number of items per page
     *
     * @param	array	$filter		Filter values
     * @param	string	$name		filter form name
     * @param	string	$anchor		anchor in destination page of filter, the can be a Widget_Tab id
     *
     * @return Widget_Filter
     */
    public function advancedFilterPanel($filter = null, $name = null)
    {
        $W = bab_Widgets();

        $filterPanel = $W->Filter();
        $filterPanel->setLayout($W->VBoxLayout());
        if (isset($name)) {
            $filterPanel->setName($name);
        }

        $pageLength = $this->getPageLength();
        if (null === $pageLength) {
            $pageLength = 15;
        }

        $pageNumber = $this->getCurrentPage();
        if (null === $pageNumber) {
            $pageNumber = 0;
        }

        $this->setPageLength(isset($filter['pageSize']) ? $filter['pageSize'] : $pageLength);
//        $this->setCurrentPage(isset($filter['pageNumber']) ? $filter['pageNumber'] : $pageNumber);

        if (isset($name)) {
            $this->sortParameterName = $name . '[filter][sort]';
        } else {
            $this->sortParameterName = 'filter[sort]';
        }

        if (isset($filter['sort'])) {
            $this->setSortField($filter['sort']);
        } elseif (!isset($this->sortField)) {

            if (method_exists($this, 'getDefaultSortField'))
            {
                $this->setSortField($this->getDefaultSortField());
            } else {
                $columns = $this->getVisibleColumns();
                list($sortField) = each($columns);
                $this->setSortField($sortField);
            }
        }

        $form = $this->getAdvancedFilterForm(null, $filter);

        if (isset($filter)) {
            if (isset($name)) {
                $path = array($name, 'filter');
            } else {
                $path = array('filter');
            }
            $form->setValues($filter, $path);
        }

        $filterPanel->setFilter($form);
        $filterPanel->setFiltered($this);

        return $filterPanel;
    }
}

/**
 * Creates a specific filter panel containing the table and an auto-generated form.
 *
 * @param Func_App                     $App
 * @param app_TableModelView           $tableview
 * @param Widget_Displayable_Interface $formContent
 * @param array                        $filter
 * @param string                       $name
 * @return Widget_Filter
 */
function app_filteredTableView(Func_App $App, app_TableModelView $tableview, Widget_Displayable_Interface $formContent, $filterValues = null, $name = null)
{
    $W = bab_Widgets();

    $filterPanel = $W->Filter();
    $filterPanel->setLayout($W->VBoxLayout());
    if (isset($name)) {
        $filterPanel->setName($name);
    }

    if (isset($filterValues['pageSize'])) {
        $tableview->setPageLength($filterValues['pageSize']);
    } else if ($tableview->getPageLength() === null) {
        $tableview->setPageLength(12);
    }

    $tableview->setCurrentPage(isset($filterValues['pageNumber']) ? $filterValues['pageNumber'] : 0);

    $tableview->sortParameterName = $name . '[filter][sort]';

    if (isset($filterValues['sort'])) {
        $tableview->setSortField($filterValues['sort']);
    } else {
        $columns = $tableview->getVisibleColumns();
        foreach ($columns as $sort => $column) {
            if ($sortField = $column->getField()) {
                $tableview->setSortField($sort);
                break;
            }
        }
    }


    if ($formContent instanceof Widget_Form) {
        $form = $formContent;
    } else {
        $form = $W->Form()->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $form->setReadOnly(true);
        $form->setName('filter');
        $form->colon();
        $form->addItem($formContent);

        $form->addItem($W->SubmitButton()->setLabel($App->translate('Filter')));
    }


    if (isset($filterValues) && is_array($filterValues)) {
        $form->setValues($filterValues, array($name, 'filter'));
    }
    $form->setHiddenValue('tg', bab_rp('tg'));
    $form->setHiddenValue('idx', bab_rp('idx'));

    $filterPanel->setFilter($form);
    $filterPanel->setFiltered($tableview);

    return $filterPanel;
}



/**
 * @param string|ORM_Field $field
 * @param string|null $description
 *
 * @return widget_TableModelViewColumn
 */
function app_TableModelViewColumn($field, $description = null)
{
    if (null === $field) {
        return null;
    }

    if (!isset($description) && $field instanceof ORM_Field) {
        $App = $field->getParentSet()->App();
        $description = $App->translate($field->getDescription());
    }

    return new widget_TableModelViewColumn($field, $description);
}
