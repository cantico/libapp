<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

$W = bab_Widgets();
$W->includePhpClass('Widget_BabPage');


/**
 * Constructs a app_Page.
 *
 * @param 	string			$id			The item unique id.
 * @param	Widget_Layout	$layout
 * @return app_Page
 */
function app_Page($id = null, Widget_Layout $layout = null)
{
    return new app_Page($id, $layout);
}


/**
 * A app_SuggestOrganization
 */
class app_Page extends Widget_BabPage
{

    private $app = null;

    /**
     * @var Widget_Layout
     */
    private $mainPanel = null;

    /**
     * @var Widget_Frame
     */
    protected $context = null;


    private $toolbars;


    public function __construct($id = null, Widget_Layout $layout = null)
    {
        parent::__construct($id, $layout);

        /* @var $I Func_Icons */
        $I = bab_Functionality::get('Icons');
        if ($I) {
            $I->includeCss();
        }

        $this->addClass('app-page');
    }


    /**
     * @param Func_App $app
     *
     * @return app_Page
     */
    public function setApp(Func_App $app)
    {
        $this->app = $app;
        return $this;
    }


    /**
     * @return Func_App
     */
    public function App()
    {
        return $this->app;
    }


    /**
     * Sets the layout for the page main panel.
     *
     * @param Widget_Layout $layout
     *
     * @return app_Page
     */
    public function setMainPanelLayout(Widget_Layout $layout)
    {
        $this->mainPanel = $layout;
        return $this;
    }


    /**
     * Add item to page
     * @param	Widget_Item $item
     * @return	app_Page
     */
    public function addItem(Widget_Displayable_Interface $item = null, $order = null)
    {
        if (!isset($this->mainPanel)) {
            $W = bab_widgets();
            $this->mainPanel = $W->VBoxLayout();
        }
        $this->mainPanel->addItem($item, $order);
        $this->mainPanel->setParent($this);
        return $this;
    }


    /**
     * Add the item to the context panel of the page
     * @param	Widget_Item $item
     * @param	string		$title		An optional title which, if specified, will create a section title and append the item below.
     * @return	app_Page
     */
    public function addContextItem(Widget_Displayable_Interface $item, $title = null, $position = null)
    {
        $W = bab_widgets();

        if (null === $this->context) {

            $isAjaxRequest = (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
                && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
            $contextPanelId = $isAjaxRequest ? null : 'app_context_panel';

            $this->context = $W->Frame(
                $contextPanelId,
                $W->VBoxLayout()->setVerticalSpacing(2, 'em')
            );
        }
        if (isset($title)) {
            $this->context->addItem(
                $W->Section(
                    $title,
                    $item,
                    4
                ),
                $position
            );
        } else {
            $this->context->addItem($item, $position);
        }
        return $this;
    }


    /**
     * Add the item to the context panel of the page
     * @param	Widget_Item $item
     * @return	app_Page
     */
    public function addToolbar(Widget_Displayable_Interface $item)
    {
        $W = bab_widgets();

        if (null === $this->toolbars) {
            $this->toolbars = $W->Frame(
                null,
                $W->VBoxItems()
            );
        }
        $this->toolbars->addItem($item);

        return $this;
    }

    protected function getMainPanel()
    {
        $W = bab_widgets();

        $mainPanel = $W->Frame(null, $this->mainPanel);
        if (bab_isAjaxRequest()) {
            $mainPanel->addClass('app_main_panel');
        } else {
            $mainPanel->setId('app_main_panel');
        }

        return $mainPanel;
    }


    /**
     * Get main panel and context
     *
     * @param Widget_Item $mainPanel
     * @return Widget_Item
     */
    protected function getMainPanelAndContext()
    {
        $mainPanel = $this->getMainPanel();

        if ($this->context) {

            $W = bab_widgets();

            $mainPanel = $W->HBoxItems(
                $mainPanel->setSizePolicy('app-main-panel'),
                $this->context->setSizePolicy('app-context-panel')
            )->addClass('app-two-panel-box');

            $this->addClass('app-two-panel-page');
        }

        $mainPanel = $this->addToolbars($mainPanel);

        return $mainPanel;
    }


    protected function addToolbars(Widget_Item $mainPanel)
    {
        if ($this->toolbars) {

            $W = bab_widgets();

            $mainPanel = $W->VBoxItems(
                $this->toolbars,
                $mainPanel
                ->setSizePolicy(Widget_SizePolicy::MAXIMUM)
            );
        }

        return $mainPanel;
    }


    /**
     * @param Widget_Canvas $canvas
     * @return string
     */
    public function display(Widget_Canvas $canvas)
    {
        $W = bab_functionality::get('Widgets');

        $mainPanel = $this->getMainPanelAndContext();



        $this->setLayout($mainPanel);

        return parent::display($canvas);
    }


    /**
     * @return widget_VBoxLayout
     */
    public function ActionsFrame()
    {
        $W = bab_functionality::get('Widgets');
        return $W->VBoxLayout()
            ->setVerticalSpacing(1, 'px')
            ->addClass('icon-left-16 icon-16x16 icon-left')
            ->addClass('app-actions');
    }



    /**
     * Add message info to page if necessary
     */
    public function addMessageInfo()
    {
        $W = bab_Widgets();

        if (isset($_SESSION['app_msginfo'])) {

            // all widget have static id to not change id of others widgets in page

            $infoMessages =  $_SESSION['app_msginfo'];
            foreach ($infoMessages as $infoLine) {
                $this->App()->Controller()->addMessage($infoLine);
            }

            unset($_SESSION['app_msginfo']);
        }
    }



    public function setNumberFormat($str)
    {
        /**
         * Add page metadata
         * @see	app.jquery.js#window.babAddonLibApp.numberFormat
         */
        $this->setMetadata('numberFormatSeparator', mb_substr($str, 1, 1));
        $this->setMetadata('numberFormatPrecision', mb_strlen($str) -2);
    }
}
