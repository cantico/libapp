<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/controller.class.php';



abstract class app_CtrlRecord extends app_Controller
{
    /**
     * This methods returns the Record classname managed by this controller.
     * The default method guess the record classname based on the controller name.
     * It can be overriden by inherited controllers.
     *
     * @return string
     */
    protected function getRecordClassName()
    {
        list(, $recordClassname) = explode('_Ctrl', $this->getClass());
        return $recordClassname;
    }


    /**
     * This methods returns the Record set managed by this controller.
     * The default method guess the record set based on the controller name.
     * It can be overriden by inherited controllers.
     *
     * @return app_RecordSet
     */
    protected function getRecordSet()
    {
        $App = $this->App();
        $recordClassname = $this->getRecordClassName();
        $recordSetClassname = $recordClassname . 'Set';

        $recordSet = $App->$recordSetClassname();
        return $recordSet;
    }


    /**
     * @return ORM_RecordSet
     */
    protected function getEditRecordSet()
    {
        return $this->getRecordSet();
    }


    /**
     * This methods returns the Record set used to save records
     *
     * @return ORM_RecordSet
     */
    protected function getSaveRecordSet()
    {
        return $this->getRecordSet();
    }

    /**
     * This methods returns the Record set used to delete records
     *
     * @return ORM_RecordSet
     */
    protected function getDeleteRecordSet()
    {
        return $this->getSaveRecordSet();
    }



    /**
     *
     * @return string[]
     */
    protected function getAvailableModelViewTypes()
    {
        $App = $this->App();
        $Ui = $App->Ui();

        $types = array();

        $recordClassname = $this->getRecordClassName();

        $viewClassname =  $recordClassname . 'TableView';
        if (method_exists($Ui, $viewClassname)) {
            $types['table'] = array(
                'classname' => $viewClassname,
                'icon' => Func_Icons::ACTIONS_VIEW_LIST_TEXT,
                'label' => $App->translate('Detailed list')
            );
        }

        $viewClassname =  $recordClassname . 'CardsView';
        if (method_exists($Ui, $viewClassname)) {
            $types['cards'] = array(
                'classname' => $viewClassname,
                'icon' => 'actions-view-list-cards', //Func_Icons::ACTIONS_VIEW_LIST_CARDS
                'label' => $App->translate('Cards')
            );
        }

        $viewClassname =  $recordClassname . 'MapView';
        if (method_exists($Ui, $viewClassname)) {
            $types['map'] = array(
                'classname' => $viewClassname,
                'icon' => Func_Icons::APPS_PREFERENCES_SITE,
                'label' => $App->translate('Map')
            );
        }

        return $types;
    }



    /**
     * @param string $itemId
     * @return string
     */
    public function getModelViewDefaultId($itemId = null)
    {
        if (!isset($itemId)) {
            $itemId = $this->getClass() . '__modelView';
        }

        return $itemId;
    }

    /**
     * Returns the xxxModelView associated to the RecordSet.
     *
     * @param array|null    $filter
     * @param string        $type
     * @param array|null    $columns    Optional list of columns. array($columnPath] => '1' | '0').
     * @param string|null   $itemId     Widget item id
     * @return app_TableModelView
     */
    protected function modelView($filter = null, $type = null, $columns = null, $itemId = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();

        $recordSet = $this->getRecordSet();

        $recordClassname = $this->getRecordClassName();

        $itemId = $this->getModelViewDefaultId($itemId);

        if (!isset($type)) {
            $type = $this->getFilteredViewType($itemId);
        }

        //$types = $this->getAvailableModelViewTypes();

        switch ($type) {
            case 'cards':
                $tableviewClassname =  $recordClassname . 'CardsView';
                break;

            case 'map':
                $tableviewClassname =  $recordClassname . 'MapView';
                break;

            case 'table':
            default:
                $tableviewClassname =  $recordClassname . 'TableView';
                break;
        }


        /* @var $tableview widget_TableModelView */
        $tableview = $Ui->$tableviewClassname();

        $tableview->setRecordController($this);

        $tableview->setId($itemId);

        $tableview->setRecordSet($recordSet);
        $tableview->addDefaultColumns($recordSet);
        if (isset($filter)) {
            $tableview->setFilterValues($filter);
        }
        $filter = $tableview->getFilterValues();

        if (isset($filter['showTotal']) && $filter['showTotal']) {
            $tableview->displaySubTotalRow(true);
        }

        $conditions = $tableview->getFilterCriteria($filter);

        $conditions = $conditions->_AND_(
            $recordSet->isReadable()
        );

        $records = $recordSet->select($conditions);


        $tableview->setDataSource($records);



        if (isset($columns)) {
            $availableColumns = $tableview->getVisibleColumns();

            $remainingColumns = array();
            foreach ($availableColumns as $availableColumn) {
                $colPath = $availableColumn->getFieldPath();
                if (isset($columns[$colPath]) && $columns[$colPath] == '1') {
                    $remainingColumns[] = $availableColumn;
                }
            }
            $tableview->setColumns($remainingColumns);
        }

        $tableview->allowColumnSelection();

        return $tableview;
    }




    /**
     * @return app_Editor
     */
    protected function recordEditor($itemId = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();

        $recordClassname = $this->getRecordClassName();
        $editorClassname =  $recordClassname . 'Editor';
        $editor = $Ui->$editorClassname();
        if (!isset($itemId)) {
            $itemId = $this->getClass() . '.' . __FUNCTION__;
        }
        $editor->setId($itemId);
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setSaveAction($this->proxy()->save());
        $editor->setName('data');

        $editor->isAjax = bab_isAjaxRequest();

        return $editor;
    }



    /**
     * @param widget_TableModelView $tableView
     * @return app_Toolbar
     */
    protected function toolbar(widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $proxy = $this->proxy();

        $filter = $tableView->getFilterValues();

        $toolbar = $W->FlowItems();
        $toolbar->addClass('widget-toolbar', Func_Icons::ICON_LEFT_16);

        $viewTypes = $this->getAvailableModelViewTypes();

        if (count($viewTypes) > 1) {
            $viewsBox = $W->Items();
            $viewsBox->setSizePolicy('pull-right');
//            $viewsBox->addClass('btn-group', Func_Icons::ICON_LEFT_16);
            $toolbar->addItem($viewsBox);

            $filteredViewType = $this->getFilteredViewType($tableView->getId());

            foreach ($viewTypes as $viewTypeId => $viewType) {
                $viewsBox->addItem(
                    $W->Link(
                        '',
                        $proxy->setFilteredViewType($tableView->getId(), $viewTypeId)
                    )->addClass('icon', $viewType['icon'] . ($filteredViewType=== $viewTypeId ? ' active' : ''))
//                    ->setSizePolicy('btn btn-xs btn-default ' . ($filteredViewType === $viewTypeId ? 'active' : ''))
                    ->setTitle($viewType['label'])
                    ->setAjaxAction()
                );
            }
        }

        if (method_exists($proxy, 'exportSelect')) {
            $toolbar->addItem(
                $W->Link(
                    $App->translate('Export'),
                    $proxy->exportSelect($filter)
                )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_DOWNLOAD)
                ->setOpenMode(Widget_Link::OPEN_DIALOG)
            );
        }

        if (method_exists($proxy, 'printList')) {
            $toolbar->addItem(
                $W->Link(
                    $App->translate('Print'),
                    $proxy->printList($filter)
                )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_PRINT)
                ->setOpenMode(Widget_Link::OPEN_POPUP)
            );
        }

        return $toolbar;
    }




    /**
     * @param string $itemId
     * @return boolean
     */
    public function toggleFilterVisibility($itemId)
    {
        $W = bab_Widgets();

        $filterVisibility = $this->getFilterVisibility($itemId);
        $filterVisibility = !$filterVisibility;
        $W->setUserConfiguration($itemId . '/filterVisibility', $filterVisibility);

        return true;
    }


    /**
     * @param string $itemId
     * @return boolean
     */
    protected function getFilterVisibility($itemId)
    {
        $W = bab_Widgets();
        $filterVisibility = $W->getUserConfiguration($itemId . '/filterVisibility');
        if (!isset($filterVisibility)) {
            $filterVisibility = false;
            $W->setUserConfiguration($itemId . '/filterVisibility', $filterVisibility);
        }
        return $filterVisibility;
    }


    /**
     * Returns the current filtered view type (table, cards...)
     * @param string        $itemId     The model view widget id
     * @return string
     */
    protected function getFilteredViewType($itemId)
    {
        $W = bab_Widgets();
        $type = $W->getUserConfiguration($itemId . '/viewType');
        if (!isset($type)) {
            $type = 'table';
        }
        return $type;
    }


    /**
     * Sets the current filtered view type (table, cards...)
     * @param string        $itemId     The model view widget id
     * @param string        $type       'table, 'cards'...
     */
    public function setFilteredViewType($itemId, $type = null)
    {
        $W = bab_Widgets();
        $W->setUserConfiguration($itemId . '/viewType', $type);

        return true;
    }


    /**
     * @param array     $filter
     * @param string    $type
     * @param string    $itemId
     */
    public function filteredView($filter = null, $type = null, $itemId = null)
    {
        $W = bab_Widgets();

        $view = $this->modelView($filter, $type, null, $itemId);
        $view->setAjaxAction();

        $filter = $view->getFilterValues();

         $filterPanel = $view->advancedFilterPanel($filter);

        if ($this->getFilterVisibility($itemId)) {
            $filterPanel->addClass('show-filter');
        } else {
            $filterPanel->addClass('hide-filter');
        }
        $toolbar = $this->toolbar($view);

        $box = $W->VBoxItems(
            $toolbar,
            $filterPanel
        );
        $box->setReloadAction($this->proxy()->filteredView(null, null, $view->getId()));

        return $box;
    }



    /**
     * @param array $filter
     * @param string $type
     * @return array
     */
    protected function getListItemMenus($filter = null, $type = null)
    {
        $App = $this->App();
        $itemMenus = array();

//         $tabs = $this->getTabs();

//         if (count($tabs) > 0) {
//             $itemMenus['all'] = array (
//                 'label' => $App->translate('All'),
//                 'action' => $this->proxy()->setCurrentTab('all')
//             );
//         }
//         foreach ($tabs as $tab) {
//             $itemMenus[$tab] = array(
//                 'label' => $tab,
//                 'action' => $this->proxy()->setCurrentTab($tab)
//             );
//         }

        return $itemMenus;
    }



    /**
     * @param array	$filter
     * @param string $type
     * @return Widget_Page
     */
    public function displayList($filter = null, $type = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $W = bab_Widgets();

        $page = $W->BabPage();
        $page->addClass('app-page-list');

        $itemMenus = $this->getListItemMenus();

        foreach ($itemMenus as $itemMenuId => $itemMenu) {
            $page->addItemMenu($itemMenuId, $itemMenu['label'], $itemMenu['action']);
        }

        $filteredView = $this->filteredView($filter, $type);

        $page->addItem($filteredView);

        return $page;
    }


    /**
     *
     * @param array $filter
     * @return Widget_Page
     */
    public function exportSelect($filter = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();

        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');

        $page->setTitle($App->translate('Export list'));

        $tableview = $this->modelView($filter, 'table');

        $editor = $Ui->ExportSelectEditor(__METHOD__, $tableview, $filter);

        $editor->setSaveAction(
            $this->proxy()->exportList(),
            $App->translate('Export')
        );
        $page->addItem($editor);

        return $page;
    }





    /**
     * @param	array	$filter
     */
    public function exportList($filter = null, $format = 'csv', $columns = null, $inline = true, $filename = 'export')
    {
        $App = $this->App();

        $page = $App->Ui()->Page();
        $page->addClass('app-page-list');

        $tableview = $this->modelView($filter, 'table', $columns);
        $tableview->allowColumnSelection(false);

        switch ($format) {
            case 'xlsx':
                $tableview->downloadXlsx($filename . '.xlsx');
            case 'xls':
                $tableview->downloadExcel($filename . '.xls');
            case 'ssv':
                $tableview->downloadCsv($filename . '.csv', ';', $inline, 'Windows-1252');
            case 'csv':
            default:
                $tableview->downloadCsv($filename . '.csv', ',', $inline, bab_charset::getIso());
        }
    }


    /**
     * Returns a page with the record information.
     *
     * @param int	$id
     * @return app_Page
     */
    public function display($id, $view = '')
    {
        $App = $this->App();
        $page = $App->Ui()->Page();

        $recordSet = $this->getRecordSet();

        $record = $recordSet->request($id);

        if (!$record->isReadable()) {
            throw new app_AccessException($App->translate('You do not have access to this page.'));
        }

        $W = bab_Widgets();
        $Ui = $App->Ui();

        $recordClassname = $this->getRecordClassName();
        $fullFrameClassname =  $recordClassname . 'FullFrame';
        $fullFrame = $Ui->$fullFrameClassname($record, $view);


        $maxHistory = 8;
        $historyFrame = $W->VBoxItems(
            $App->Controller()->Search(false)->history($record->getRef(), $maxHistory, 0, true)
        );
        $fullFrame->addItem(
            $W->Section(
                $App->translate('History'),
                $historyFrame,
                2
            )->setFoldable(true)
            ->addClass('compact')
        );

        $page->addItem($fullFrame);


        // Actions
        $page->addContextItem($this->getDisplayActionFrame($page, $record));

        return $page;
    }



    protected function getDisplayActionFrame(Widget_Page $page, app_Record $record)
    {
        $actionsFrame = $page->ActionsFrame();
        return $actionsFrame;
    }



    /**
     * Returns a page containing an editor for the record.
     *
     * @param string|null id    A record id or null for a new record editor.
     * @return Widget_Page
     */
    public function edit($id = null, $view = '')
    {
        $W = bab_Widgets();
        $App = $this->App();

        $recordSet = $this->getEditRecordSet();

        $page = $W->BabPage();
        $page->addClass('app-page-editor');

        $page->setTitle($App->translate($recordSet->getDescription()));

        $editor = $this->recordEditor();

        if (isset($id)) {
            $record = $recordSet->request($id);
            if (!$record->isUpdatable()) {
                throw new app_AccessException($App->translate('You do not have access to this page.'));
            }
        } else {
            if (!$recordSet->isCreatable()) {
                throw new app_AccessException($App->translate('You do not have access to this page.'));
            }
            $record = $recordSet->newRecord();
        }
        $editor->setRecord($record);

        $page->addItem($editor);

        return $page;
    }

    /**
     * Returns a page containing an editor for the tags record.
     *
     * @param string|null id    A record id or null for a new record editor.
     * @return Widget_Page
     */
    public function editTags($id = null, $view = '')
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $recordSet = $this->getEditRecordSet();

        $page = $W->BabPage();
        $page->addClass('app-page-editor');

        $page->setTitle($App->translate($recordSet->getDescription()));

        $editor = $Ui->TagsEditor();

        if (isset($id)) {
            $record = $recordSet->request($id);
            if (!$record->isUpdatable()) {
                throw new app_AccessException($App->translate('You do not have access to this page.'));
            }
            $editor->setRecord($record);
        } else {
            if (!$recordSet->isCreatable()) {
                throw new app_AccessException($App->translate('You do not have access to this page.'));
            }
            $record = $recordSet->newRecord();
            $editor->setRecord($record);
        }

        $page->addItem($editor);

        return $page;
    }


    public function ok()
    {
        return true;
    }



    /**
     *
     * @param  $data
     */
    public function create($data)
    {

    }


    /**
     *
     * @param  $data
     */
    public function update($data)
    {

    }



    /**
     *
     */
    public function read($id)
    {

    }


    /**
     * Get the message to display on delete next page
     * @return string | true
     */
    protected function getDeletedMessage(ORM_Record $record)
    {
        $App = $this->App();
        $recordSet = $this->getRecordSet();

        $recordTitle = $record->getRecordTitle();

        $message = sprintf($App->translate('%s has been deleted'), $App->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"');
        return $message;
    }

    /**
     * Get the message to display on create next page
     * @return string | true
     */
    protected function getCreatedMessage()
    {
        return true;
    }

    /**
     * Get the message to display on modification next page
     * @param app_Record $record The record before the modification
     * @return string | true
     */
    protected function getModifedMessage(ORM_Record $record)
    {
        return true;
    }


    /**
     * Method to check reord before saving
     * @param app_Record $record
     */
    protected function preSave(ORM_Record $record, $data)
    {

    }


    /**
     * Method for post save actions on record
     * @param app_Record $record
     */
    protected function postSave(ORM_Record $record, $data)
    {

    }


    /**
     * Save a record
     *
     * @requireSaveMethod
     *
     * @param array $data
     */
    public function save($data = null)
    {
        $this->requireSaveMethod();
        $App = $this->App();
        $recordSet = $this->getSaveRecordSet();
        $pk = $recordSet->getPrimaryKey();
        $message = null;

        if (!empty($data[$pk])) {
            $record = $recordSet->request($data[$pk]);
            unset($data[$pk]);

            if (!$record->isUpdatable()) {
                throw new app_AccessException('Access denied');
            }

            $message = $this->getModifedMessage($record);
        } else {

            $record = $recordSet->newRecord();

            if (!$recordSet->isCreatable()) {
                throw new app_AccessException('Access denied');
            }

            $message = $this->getCreatedMessage();
        }

        if (!isset($record)) {
            throw new app_SaveException($App->translate('The record does not exists'));
        }


        if (!isset($data)) {
            throw new app_SaveException($App->translate('Nothing to save'));
        }

        $record->setFormInputValues($data);

        $this->preSave($record, $data);
        if ($record->save()) {
            $this->postSave($record, $data);
            if (is_string($message)) {
                $this->addMessage($message);
            }
            return true;
        }

        return false;
    }


    /**
     * Deletes the specified record.
     *
     * @requireDeleteMethod
     *
     * @param string $id        The record id
     * @return boolean
     */
    public function delete($id)
    {
        $this->requireDeleteMethod();
        $recordSet = $this->getDeleteRecordSet();

        $record = $recordSet->request($id);

        if (!$record->isDeletable()) {
            throw new app_AccessException('Sorry, You are not allowed to perform this operation');
        }
        $deletedMessage = $this->getDeletedMessage($record);

        if ($record->delete()) {
            $this->addMessage($deletedMessage);
        }

        return true;
    }


    /**
     * Displays a page asking to confirm the deletion of a record.
     *
     * @param int $id
     */
    public function confirmDelete($id)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->get($id);

        $page = $Ui->Page();

        $page->addClass('app-page-editor');
        $page->setTitle($App->translate('Deletion'));

        $form = new app_Editor($App);

        $form->addItem($W->Hidden()->setName('id'));

        $recordTitle = $record->getRecordTitle();

        $subTitle = $App->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));

        $form->addItem($W->Title($App->translate('Confirm delete?'), 6));

        $form->addItem($W->Html(bab_toHtml($App->translate('It will not be possible to undo this deletion.'))));

        $confirmedAction = $this->proxy()->delete($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
                ->setAjaxAction($confirmedAction)
                ->setLabel($App->translate('Delete'))
         );
        $form->addButton($W->SubmitButton()->setLabel($App->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);

        return $page;
    }

    protected function getClass()
    {
        return get_class($this);
    }
}
