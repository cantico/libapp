<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

$App = app_App();
$App->includeRecordSet();


/**
 * An arbitrary link between two records.
 *
 * @property ORM_StringField    $sourceClass
 * @property ORM_StringField    $sourceId
 * @property ORM_StringField    $targetClass
 * @property ORM_StringField    $targetId
 * @property ORM_StringField    $type
 *
 * @method app_Link                  get()
 * @method app_Link                  request()
 * @method app_Link[]|\ORM_Iterator  select()
 * @method app_Link                  newRecord()
 * @method Func_App App()
 */
class app_LinkSet extends app_RecordSet
{
    /**
     * @param Func_App $App
     */
    public function __construct(Func_App $App = null)
    {
        parent::__construct($App);

        $App = $this->App();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('sourceClass')
                ->setDescription('Source object class'),
            ORM_StringField('sourceId')
                ->setDescription('Source object id'),
            ORM_StringField('targetClass')
                ->setDescription('Target object class'),
            ORM_StringField('targetId')
                ->setDescription('target object id'),
            ORM_StringField('type')
                ->setDescription('Link type')
        );
    }



	/**
	 * @param	string	$sourceClass
	 */
	public function joinSource($sourceClass)
	{
	    if (get_class($this->sourceId) !== $sourceClass . 'Set') {
    	    $this->hasOne('sourceId', $sourceClass . 'Set');
    		$this->join('sourceId');
	    }
	}


	/**
	 * @param	string	$targetClass
	 */
	public function joinTarget($targetClass = null)
	{
	    if (get_class($this->targetId) !== $targetClass . 'Set') {
    		$this->hasOne('targetId', $targetClass . 'Set');
    		$this->join('targetId');
	    }
	}



	public function sourceIsA($recordClass)
	{
		return $this->sourceClass->is($recordClass);
	}

	/**
	 *
	 * @return ORM_Iterator
	 */
	public function selectForSource(app_Record $object, $targetClass = null, $linkType = null)
	{
		$criteria = $this->sourceId->is($object->id)
            ->_AND_($this->sourceClass->is(get_class($object)));

		if (isset($targetClass)) {
		    $this->joinTarget($targetClass);
		    $criteria = $criteria->_AND_($this->targetClass->is($targetClass));
		}

		if (isset($linkType)) {
			if (is_array($linkType)) {
				$criteria = $criteria->_AND_($this->type->in($linkType));
			} else {
				$criteria = $criteria->_AND_($this->type->is($linkType));
			}
		}

		if (is_a($this->targetId, 'app_TraceableRecordSet')) {
			$criteria = $criteria->_AND_($this->targetId->deleted->is(false));
		}

		return $this->select($criteria);
	}


	/**
	 *
	 * @return ORM_Iterator
	 */
	public function selectForSources($objects, $targetClass, $linkType = null)
	{
		$sourceClass = null;
		$sourceIds = array();

		foreach ($objects as $obj) {
			if (is_null($sourceClass)) {
				$sourceClass = get_class($obj);
			}
			$sourceIds[] = $obj->id;
		}
		$criteria = $this->sourceId->in($sourceIds)
			->_AND_($this->sourceClass->is($sourceClass));

		if (isset($targetClass)) {
		    $this->joinTarget($targetClass);
		    $criteria = $criteria->_AND_($this->targetClass->is($targetClass));
		}

		if (isset($linkType)) {
			if (is_array($linkType)) {
				$criteria = $criteria->_AND_($this->type->in($linkType));
			} else {
				$criteria = $criteria->_AND_($this->type->is($linkType));
			}
		}
		return $this->select($criteria);
	}


	/**
	 *
	 * @return ORM_Iterator
	 */
	public function selectForTarget(app_Record $object, $sourceClass = null, $linkType = null)
	{
		$criteria = $this->targetId->is($object->id)
			->_AND_($this->targetClass->is(get_class($object)));

		if (isset($sourceClass)) {
		    $this->joinSource($sourceClass);
			$criteria = $criteria->_AND_($this->sourceClass->is($sourceClass));
		}

		if (isset($linkType)) {
			if (is_array($linkType)) {
				$criteria = $criteria->_AND_($this->type->in($linkType));
			} else {
				$criteria = $criteria->_AND_($this->type->is($linkType));
			}
		}

		return $this->select($criteria);
	}


	/**
	 *
	 * @return ORM_Iterator
	 */
	public function selectForTargets($objects, $sourceClass = null, $linkType = null)
	{
		$targetClass = null;
		$targetIds = array();

		foreach ($objects as $obj) {
			if (is_null($targetClass)) {
				$targetClass = get_class($obj);
			}
			$targetIds[] = $obj->id;
		}
		$criteria = $this->targetId->in($targetIds)
			->_AND_($this->targetClass->is($targetClass));

		if (isset($sourceClass)) {
		    $this->joinSource($sourceClass);
			$criteria = $criteria->_AND_($this->sourceClass->is($sourceClass));
		}

		if (isset($linkType)) {
			if (is_array($linkType)) {
				$criteria = $criteria->_AND_($this->type->in($linkType));
			} else {
				$criteria = $criteria->_AND_($this->type->is($linkType));
			}
		}

		return $this->select($criteria);
	}



	/**
	 * delete all links to an object
	 *
	 * @param	app_Record		$object
	 * @param	string			$targetClass 	if target class is set, links will be deleted only for target classes
	 * @param	bool			$deleteTarget	if set to true, the target will be deleted to
	 */
	public function deleteForSource(app_Record $object, $targetClass = null, $deleteTarget = false, $linkType = null)
	{
		$set = clone $this;
		$App = $object->App();

		$criteria = $set->sourceId->is($object->id)->_AND_(
			$set->sourceClass->is(get_class($object))
		);

		if (null !== $targetClass) {
			$criteria = $criteria->_AND_(
				$set->targetClass->is($targetClass)
			);
		}
		if (null !== $linkType) {
			$criteria = $criteria->_AND_(
				$set->type->is($linkType)
			);
		}

		if ($deleteTarget) {
			foreach($set->select($criteria) as $link) {

				$className = $link->targetClass.'Set';

				// remove prefix

				$className = mb_substr($className, 1 + mb_strpos($className, '_'));
				$targetSet = $App->$className();

				$targetSet->delete($targetSet->id->is($link->targetId));
			}
		}

		return $set->delete($criteria);
	}


    /**
     * Deletes links between two objects
     *
     * @param app_Record	$source
     * @param app_Record	$target
     * @param string		$linkType
     * @return app_LinkSet
     */
    public function deleteLink($source, $target, $linkType = null)
    {
        $criteria = $this->sourceId->is($source->id)->_AND_(
            $this->sourceClass->is(get_class($source))
        )->_AND_(
            $this->targetId->is($target->id)
        )->_AND_(
            $this->targetClass->is(get_class($target))
        );
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($this->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($this->type->is($linkType));
            }
        }

        $this->delete($criteria);
        return $this;
    }
}


/**
 * An arbitrary link between two records.
 *
 * @property string		$sourceClass
 * @property string		$sourceId
 * @property string		$targetClass
 * @property string		$targetId
 * @property string		$type
 *
 * @method app_LinkSet getParent()
 * @method Func_App App()
 */
class app_Link extends app_Record
{

    /**
     * @return app_Record
     */
    public function getSource()
    {
        $App = $this->App();
        $object = substr($this->sourceClass, strlen($App->classPrefix));

        /* @var $set app_RecordSet */
        $set = $App->{$object.'Set'}();
        return $set->get($this->sourceId);
    }

    /**
     * @return app_Record
     */
    public function getTarget()
    {
        $App = $this->App();
        $object = substr($this->sourceClass, strlen($App->classPrefix));

        /* @var $set app_RecordSet */
        $set = $App->{$object.'Set'}();
        return $set->get($this->targetId);
    }
}

