<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

if (!class_exists('Widget_Action')) {
    bab_widgets()->includePhpClass('Widget_Action');
}


require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

/**
 * Translates the string.
 *
 * @param string $str
 * @return string
 */
function app_translate($str)
{
    $translation = bab_translate($str, 'LibApp');

    return $translation;
}

/**
 * Translates all the string in an array and returns a new array.
 *
 * @param array $arr
 * @return array
 */
function app_translateArray($arr)
{
    $newarr = $arr;

    foreach ($newarr as &$str) {
        $str = app_translate($str);
    }
    return $newarr;
}



/**
 * Instanciates the App factory.
 *
 * @return Func_App
 */
function app_App()
{
    return bab_functionality::get('App');
}


/**
 * Instanciates the Newsletter factory.
 *
 * @return Func_Newsletter
 */
/*function app_Newsletter()
{
    return bab_functionality::get('Newsletter');
}*/


/**
 * Instanciates the MailingList factory.
 *
 * @return Func_MailingList
 */
function app_MailingList()
{
    return bab_functionality::getOriginal('MailingList');
}



function app_addPageInfo($message)
{
    $_SESSION['app_msginfo'][] = $message;
}


/**
 * Redirects to the specified url or action.
 * This function makes the current script die.
 *
 * @param Widget_Action|string $url
 */
function app_redirect($url, $message = null)
{
    global $babBody;

    if (null === $url) {
        $script = '<script type="text/javascript">';
        if (isset($message)) {
            $script .= 'alert("' . bab_toHtml($message, BAB_HTML_JS) . '");';
        }
        $script .= 'history.back();';
        $script .= '</script>';

        die($script);
    }

    if ($url instanceof Widget_Action) {
        $url = $url->url();
    }
    if (!empty($babBody->msgerror)) {
        $url .=  '&msgerror=' . urlencode($babBody->msgerror);
    }
    if (isset($message)) {
        $lines = explode("\n", $message);
        foreach ($lines as $line) {
            app_addPageInfo($line);
        }
    }

    header('Location: ' . $url);
    die;
}





/**
 * create or validate a rewriten name
 * @param string $name
 */
function app_getRewriteName($rewriteName)
{
    $rewriteName = bab_removeDiacritics($rewriteName);
    $rewriteName = strtolower($rewriteName);
    $rewriteName = str_replace(array(' ', '/'), '-', $rewriteName);
    $rewriteName = preg_replace('/[^-0-9A-Za-z]/', '', $rewriteName);
    $rewriteName = preg_replace('/[-]+/', '-', $rewriteName);
    $rewriteName = trim($rewriteName, '- ');

    return $rewriteName;
}



/**
 * Upload file error to string
 * @return string
 */
function app_fileUploadError($error)
{
    switch($error) {
        case UPLOAD_ERR_OK:
            return null;

        case UPLOAD_ERR_INI_SIZE:
            return app_translate('The uploaded file exceeds the upload_max_filesize directive.');

        case UPLOAD_ERR_FORM_SIZE:
            return app_translate('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.');

        case UPLOAD_ERR_PARTIAL:
            return app_translate('The uploaded file was only partially uploaded.');

        case UPLOAD_ERR_NO_FILE:
            return app_translate('No file was uploaded.');

        case UPLOAD_ERR_NO_TMP_DIR: //  since PHP 4.3.10 and PHP 5.0.3
            return app_translate('Missing a temporary folder.');

        case UPLOAD_ERR_CANT_WRITE: //  since php 5.1.0
            return app_translate('Failed to write file to disk.');

        default :
            return app_translate('Unknown File Error.');
    }
}





/**
 * Fix datetime submited from a separated datePicker and timePicker
 * @param array $dateTime
 * @param bool $ignoreTime
 *
 * @return string
 */
function app_fixDateTime(Array $dateTime, $ignoreTime)
{
    $W = app_Widgets();
    $date = $W->DatePicker()->getISODate($dateTime['date']);

    if (!$date || '0000-00-00' === $date) {
        return '0000-00-00 00:00:00';
    }

    $hour = $dateTime['time'];

    if (5 === mb_strlen($hour)) {
        $hour .= ':00';
    }

    if ($ignoreTime || empty($hour) || 8 !== mb_strlen($hour)) {
        $hour = '00:00:00';
    }

    return $date.' '.$hour;
}



/**
 * @param	mixed	$value
 * @return	string
 */
function app_json_encode($a)
{
    if (is_null($a)) return 'null';
    if ($a === false) return 'false';
    if ($a === true) return 'true';
    if (is_scalar($a)) {
        if (is_float($a)) {
            // Always use "." for floats.
            return floatval(str_replace(",", ".", strval($a)));
        }
        
        if (is_string($a)) {
            static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
            return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
        } else {
            return $a;
        }
    }
    require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
    if ($a instanceof BAB_DateTime) {
        /* @var $a BAB_DateTime */
        return 'new Date(Date.UTC(' . $a->getYear() . ',' . ($a->getMonth() - 1) . ',' . $a->getDayOfMonth() . ',' . $a->getHour() . ',' . $a->getMinute() . ',' . $a->getSecond() . ',0))';
    }
    if (is_object($a) && method_exists($a, 'toJson')) {
        return $a->toJson();
    }
    $isList = true;
    for ($i = 0, reset($a); $i < count($a); $i++, next($a)) {
        if (key($a) !== $i) {
            $isList = false;
            break;
        }
    }
    $result = array();
    if ($isList) {
        foreach ($a as $v) {
            $result[] = app_json_encode($v);
        }
        return '[' . join(',', $result) . ']';
    } else {
        foreach ($a as $k => $v) {
            $result[] = app_json_encode($k).':'.app_json_encode($v);
        }
        return '{' . join(',', $result) . '}';
    }
}
