<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/define.php';
require_once dirname(__FILE__) . '/functions.php';

/**
 * Provides extensible functionalities to manage an application.
 */
class Func_App extends bab_Functionality
{
    public $addonPrefix;
    public $classPrefix;
    public $addonName;
    public $controllerTg;

    /**
     * @var string
     */
    private $language = null;


    public function __construct()
    {
        $this->addonPrefix = 'libapp';
        $this->addonName = 'libapp';

        $this->classPrefix = $this->addonPrefix . '_';
        $this->controllerTg = 'addon/' . $this->addonName . '/main';

        $babDB = bab_getDB();

        $LibOrm = bab_Functionality::get('LibOrm');
        /*@var $LibOrm Func_LibOrm */

        $LibOrm->initMysql();
        ORM_RecordSet::setBackend(new ORM_MySqlBackend($babDB));
    }

    /**
     *
     * @return string
     */
    public function getUiPath()
    {
        return APP_UI_PATH;
    }

    public function getSetPath()
    {
        return APP_SET_PATH;
    }

    public function getPhpPath()
    {
        return APP_PHP_PATH;
    }


    /**
     * @return string
     *
     */
    public function getDescription()
    {
        return 'Application framework.';
    }


    /**
     * Get the addon name
     * @return string
     */
    public function getAddonName()
    {
        return $this->addonName;
    }


    /**
     * @return bab_addonInfos
     */
    public function getAddon()
    {
        return bab_getAddonInfosInstance($this->getAddonName());
    }


    /**
     * Register myself as a functionality.
     *
     */
    public static function register()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
        $functionalities = new bab_functionalities();

        $addon = bab_getAddonInfosInstance('libapp');

        $addon->registerFunctionality('App', 'app.php');
    }


    /**
     * Synchronize sql tables for all classes found in Application object
     * using methods  'includeXxxxxClassName'
     * sychronize if the two correspond methods are met and the set classname match the prefix from parameter
     * do not synchronize if the set method has a VueSet suffix, in this case the table si juste a readonly vue
     *
     * @param	string	$prefix		tables and classes prefix
     * @return bab_synchronizeSql
     */
    public function synchronizeSql($prefix)
    {
        if (!$prefix) {
            return null;
        }

        require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
        $mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
        $sql = 'SET FOREIGN_KEY_CHECKS=0;
            ';

        foreach (get_class_methods($this) as $method) {

            if (substr($method, 0, 7) === 'include' && substr($method, -3) === 'Set') {
                $incl = $method;
                $classNameMethod = substr($method, strlen('include')) . 'ClassName';


                $classname = $this->$classNameMethod();

                if ($prefix === substr($classname, 0, strlen($prefix))
                  && 'Set' === substr($classname, -3)
                  && 'ViewSet' !== substr($classname, -7)) {
                    if (method_exists($this, $incl)) {
                        $this->$incl();

                        $call = substr($classname, strlen($prefix));

                        if (class_exists($classname)) {

                            /* @var $set ORM_RecordSet */
                            $set = $this->$call();
                            if (method_exists($set, 'useLang')) {
                                // This is necessary if the recordSet constructor uses a setLang().
                                // We need to revert to multilang fields before synchronizing.
                                $set->useLang(false);
                            }
                            $sql .= $mysqlbackend->setToSql($set) . "\n";
                        }
                    }
                }
            }
        }

        require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
        $synchronize = new bab_synchronizeSql();

        $synchronize->fromSqlString($sql);

        return $synchronize;
    }



    public function getSynchronizeSql($prefix)
    {
        if (!$prefix) {
            return null;
        }

        require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
        $mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
        $sql = '';

        foreach (get_class_methods($this) as $method) {

            if (substr($method, 0, strlen('include')) === 'include' && substr($method, -strlen('Set')) === 'Set') {
                $incl = $method;
                $classNameMethod = substr($method, strlen('include')) . 'ClassName';

                $classname = $this->$classNameMethod();

                if ($prefix === substr($classname, 0, strlen($prefix))
                    && 'Set' === substr($classname, -3)
                    && 'ViewSet' !== substr($classname, -6)
                    ) {
                        if (method_exists($this, $incl)) {
                            $this->$incl();
                            $call = substr($classname, strlen($prefix));

                            if (class_exists($classname) && method_exists($this, $call)) {
                                $set = $this->$call();
                                $sql .= $mysqlbackend->setToSql($set) . "\n";
                            }
                        }
                    }
            }
        }

        return $sql;
    }



    public function includeBase()
    {
        require_once APP_PHP_PATH . 'base.class.php';
    }

    /**
     * Includes app_RecordSet class definition.
     */
    public function includeRecordSet()
    {
        require_once APP_SET_PATH . 'record.class.php';
    }


    /**
     * Includes app_TraceableRecordSet class definition.
     */
    public function includeTraceableRecordSet()
    {
        require_once APP_SET_PATH . 'traceablerecord.class.php';
    }



    /**
     * SET DEFINITION
     */

    //Log

    /**
     * Includes LogSet class definition.
     */
    public function includeLogSet()
    {
        require_once APP_SET_PATH . 'log.class.php';
    }

    /**
     * @return string
     */
    public function LogClassName()
    {
        return 'app_Log';
    }

    /**
     * @return string
     */
    public function LogSetClassName()
    {
        return $this->LogClassName() . 'Set';
    }

    /**
     * @return app_LogSet
     */
    public function LogSet()
    {
        $this->includeLogSet();
        $className = $this->LogSetClassName();
        $set = new $className($this);
        return $set;
    }


    //Link

    /**
     * Includes LinkSet class definition.
     */
    public function includeLinkSet()
    {
        require_once APP_SET_PATH . 'link.class.php';
    }

    /**
     * @return string
     */
    public function LinkClassName()
    {
        return 'app_Link';
    }

    /**
     * @return string
     */
    public function LinkSetClassName()
    {
        return $this->LinkClassName() . 'Set';
    }

    /**
     * @return app_LinkSet
     */
    public function LinkSet()
    {
        $this->includeLinkSet();
        $className = $this->LinkSetClassName();
        $set = new $className($this);
        return $set;
    }

    //CustomField

    /**
     * Includes CustomFieldSet class definition.
     */
    public function includeCustomFieldSet()
    {
        require_once APP_SET_PATH . 'customfield.class.php';
    }

    /**
     * @return string
     */
    public function CustomFieldClassName()
    {
        return 'app_CustomField';
    }

    /**
     * @return string
     */
    public function CustomFieldSetClassName()
    {
        return $this->CustomFieldClassName() . 'Set';
    }

    /**
     * @return app_CustomFieldSet
     */
    public function CustomFieldSet()
    {
        $this->includeCustomFieldSet();
        $className = $this->CustomFieldSetClassName();
        $set = new $className($this);
        return $set;
    }


    //Tag

    /**
     * Includes TagSet class definition.
     */
    public function includeTagSet()
    {
        require_once APP_SET_PATH . 'tag.class.php';
    }

    /**
     * @return string
     */
    public function TagClassName()
    {
        return 'app_Tag';
    }

    /**
     * @return string
     */
    public function TagSetClassName()
    {
        return $this->TagClassName() . 'Set';
    }

    /**
     * @return app_TagSet
     */
    public function TagSet()
    {
        $this->includeTagSet();
        $className = $this->TagSetClassName();
        $set = new $className($this);
        return $set;
    }








    /**
     * Returns the app_Record corresponding to the specified
     * reference $ref.
     *
     * @param string 	$ref	A reference string (e.g. Contact:12)
     * @return app_Record	or null if no corresponding record is found.
     */
    public function getRecordByRef($ref)
    {
        $refParts = explode(':', $ref);
        if (count($refParts) !== 2) {
            return null;
        }
        list($classname, $id) = $refParts;
        $classSet = $classname . 'Set';
        $set = $this->$classSet();
        if (isset($set)) {
            return $set->get($id);
        }
        return null;
    }

    /**
     * Returns the reference corresponding to the specified
     * app_Record $record (e.g. Contact:12 or Deal:125)
     *
     * @param app_Record	$record
     * @return string
     */
    public function getRecordRef(app_Record $record)
    {
        $fullClassName = get_class($record);
        list(, $className) = explode('_', $fullClassName);
        return $className . ':' . $record->id;
    }


    /**
     * Specifies the language to use for translation.
     *
     * If $language is null, the language is reset to
     * the current logged user language.
     *
     * @param string|null $language
     */
    public function setTranslateLanguage($language)
    {
        $this->language = $language;
    }



    /**
     * Translates the string.
     *
     * @param string $str
     * @return string
     */
    public function translate($str, $str_plurals = null, $number = null)
    {
        require_once APP_PHP_PATH . 'functions.php';
        $translation = $str;
        if ($translate = bab_functionality::get('Translate/Gettext')) {
            /* @var $translate Func_Translate_Gettext */
            $translate->setAddonName('libapp');
            $translation = $translate->translate($str, $str_plurals, $number);
        }

        return $translation;
    }

    /**
     * @param string    $str
     * @param string    $str_plurals
     * @param int       $number
     * @return string
     */
    public function translatable($str, $str_plurals = null, $number = null)
    {
        return $str;
    }

    /**
     * Translates all the string in an array and returns a new array.
     *
     * @param array $arr
     * @return array
     */
    public function translateArray($arr)
    {
        $newarr = $arr;

        foreach ($newarr as &$str) {
            $str = $this->translate($str);
        }
        return $newarr;
    }



    /**
     * Returns a link for writting an email to the specified email address.
     *
     * @param string $addr
     * @param string $subject
     * @param string $body
     *
     * @return string
     */
    public function mailTo($addr, $subject = null, $body = null)
    {
        $mailTo = 'mailto:' . $addr;
        $parameters = array();
        if (isset($subject)) {
            $parameters[] = 'subject=' . $subject;
        }
        if (isset($body)) {
            $parameters[] = 'body=' . $body;
        }
        if (!empty($parameters)) {
            $mailTo .= '?' . implode('&', $parameters);
        }

        return $mailTo;
    }



    /**
     * Format a number for display
     *
     * @param   float|string|null   $number     Numeric value with decimal
     * @return string
     */
    public function numberFormat($number, $decimals = 2)
    {
        if (is_null($number)) {
            return '#,##';
        }

        $number = number_format(floatval($number), $decimals, ',', ' ');
        return str_replace(' ', bab_nbsp(), $number);
    }


    /**
     * Format a number with an optional unit.
     *
     * If the value is >= 1000 the value is shortened and the corresponding prexif (k or M) is used.
     *
     * @param float|string|null $number
     * @param string $unitSymbol    (For example $, m2, Wh)
     * @param int $decimals
     * @return string|mixed
     */
    public function shortFormatWithUnit($number, $unitSymbol = '', $decimals = 2)
    {
        if (is_null($number)) {
            return '#,##';
        }

        $prefix = '';
        if ($number >= 1000000) {
            $number /= 1000000;
            $prefix = 'M';
        } elseif ($number >= 1000) {
            $number /= 1000;
            $prefix = 'k';
        }

        $number = number_format($number, $decimals, ',', ' ');
        return str_replace(' ', bab_nbsp(), $number . ' ' . $prefix . $unitSymbol);
    }


    /**
     * Reformat a phone number in the specified format.
     *
     * @param string    $phone      The phone number to be formatted
     * @param int       $format     The format the phone number should be formatted into
     *
     * @return string               The formatted phone number
     */
    public function phoneNumberFormat($phone, $format = null)
    {
        $PhoneNumber = bab_Functionality::get('PhoneNumber');
        if ($PhoneNumber === false) {
            return $phone;
        }

        if (!isset($format)) {
            $format = bab_registry::get('/' . $this->addonName . '/numberFormat', $PhoneNumber->getDefaultFormat());
        }
        $phoneNumberUtil = $PhoneNumber->PhoneNumberUtil();

        try {
            $phoneNumber = $phoneNumberUtil->parse($phone, 'FR');
            $phone = $phoneNumberUtil->format($phoneNumber, $format);
        } catch (\libphonenumber\NumberParseException $e) {
        }

        return $phone;
    }




    /**
     * Includes Controller class definition.
     */
    public function includeController()
    {
        require_once APP_PHP_PATH . '/controller.class.php';
    }


    /**
     * Includes RecordController class definition.
     */
    public function includeRecordController()
    {
        require_once APP_PHP_PATH . '/record.ctrl.php';
    }


    /**
     * Instanciates the controller.
     *
     * @return app_Controller
     */
    public function Controller()
    {
        $this->includeController();
        return bab_getInstance($this->classPrefix.'Controller')->setApp($this);
    }


    /**
     * Instanciates a controller class.
     *
     * @return bab_Controller
     */
    public function ControllerProxy($className, $proxy = true)
    {
        $this->includeController();

        if ($proxy) {
            return app_Controller::getProxyInstance($this, $className);
        }

        return new $className($this);
    }



    /**
     * Include class app_Ui
     *
     */
    public function includeUi()
    {
        require_once APP_UI_PATH . 'ui.class.php';
    }


    /**
     * The app_Ui object propose an access to all ui files and ui objects (widgets)
     *
     * @return app_Ui
     */
    public function Ui()
    {
        $this->includeUi();
        return bab_getInstance($this->classPrefix . 'Ui');//->setApp($this);
    }



    protected function includeAccess()
    {
        require_once WORKSPACE_PHP_PATH . '/access.class.php';
    }





    /**
     * Get upload path
     * if the method return null, no upload functionality
     *
     * @return bab_Path
     */
    public function getUploadPath()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
        return new bab_Path(bab_getAddonInfosInstance($this->getAddonName())->getUploadPath());
    }


    /**
     *
     * @param string $name
     * @param mixed $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        switch (true) {

            case substr($name, -strlen('SetClassName')) === 'SetClassName':
                $setName = substr($name, 0, strlen($name) - strlen('ClassName'));
                return $this->classPrefix . $setName;

            case substr($name, -strlen('ClassName')) === 'ClassName':
                $recordName = substr($name, 0, strlen($name) - strlen('ClassName'));
                return $this->classPrefix . $recordName;

            case substr($name, 0, strlen('include')) === 'include' && substr($name, -strlen('Set')) === 'Set':
                $fileNameBase = strtolower(substr(substr($name, 0, strlen($name) - strlen('Set')), strlen('include')));
                require_once PHP_SET_PATH . $fileNameBase . '.class.php';
                return;

            case substr($name, -strlen('Set')) === 'Set':
                $includeMethod = 'include' . $name;
                $this->$includeMethod();
                $setClassNameMethod = $name . 'ClassName';
                $className = $this->$setClassNameMethod();
                $set = new $className($this);
                return $set;

                //case method_exists($this, $name . 'Set'):
            default:
                $setName = $name . 'Set';
                $recordClassNameMethod = $name . 'ClassName';
                $recordClassName = $this->$recordClassNameMethod();
                if (isset($arguments[0])) {
                    if ($arguments[0] instanceof $recordClassName) {
                        return $arguments[0];
                    }
                    $set = $this->$setName();
                    return $set->get($arguments[0]);
                }
                return null;

                echo $name;
                $r = method_exists($this, $name . 'Set');
                var_dump($r);
                die;
        }
    }


    /**
     * Test if this App implementation handles the specified object class.
     *
     * The default is to consider that an object Xxxx implemented if the includeXxxxSet() method is
     * declared public on this App.
     *
     * @param	string	$objectClassName		App object name (eg. 'Contact' or 'CatalogItem')
     * @return bool
     */
    public function __isset($objectClassName)
    {
        if (null === $this->implementedObjects) {

            $this->implementedObjects = array();

            $className = get_class($this);
            $rClass = new ReflectionClass($className);

            foreach ($rClass->getMethods(ReflectionMethod::IS_PUBLIC) as $m) {

                // We consider object Xxxx implemented if the includeXxxxSet() method is
                // declared public on this.

                if ($m->getDeclaringClass()->name !== $className) {
                    // The method is declared on an ancestor class.
                    continue;
                }

                if (substr($m->name, 0, 7) === 'include' && substr($m->name, -3) === 'Set') {
                    $this->implementedObjects[substr($m->name, 7, -3)] = 1;
                }
            }
        }

        return isset($this->implementedObjects[$objectClassName]);
    }
}

