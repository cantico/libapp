<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


$App = app_App();
$App->includeBase();


/**
 * @method app_Record   get()
 * @method app_Record   request()
 * @method app_Record   select()
 */
class app_RecordSet extends ORM_RecordSet implements app_Object_Interface
{
    /**
     * @var Func_App
     */
    protected $app = null;

    protected $accessRights = null;

    /**
     * @var array
     */
    private $customFields = null;

    /**
     * @param Func_App $app
     */
    public function __construct(Func_App $app)
    {
        parent::__construct();
        $this->setApp($app);
        $this->accessRights = array();
    }

    /**
     * {@inheritDoc}
     * @see app_Object_Interface::setApp()
     */
    public function setApp(Func_App $app)
    {
        $this->app = $app;
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see app_Object_Interface::App()
     */
    public function App()
    {
        return $this->app;
    }

    /**
     * @param string $setName
     */
    protected function extractSetPrefixAndName($setName)
    {
        return explode('_', $setName);
    }

    /**
     * Similar to the ORM_RecordSet::join() method but instanciates the
     * joined RecordSet using App methods.
     *
     * @see ORM_RecordSet::join()
     */
    public function join($fkFieldName)
    {
        $fkField = $this->getField($fkFieldName);
        if (!($fkField instanceof ORM_FkField)) {
            return $this;
        }
        $setName = $fkField->getForeignSetName();

        if (!$setName || 'Set' === $setName) {
            throw new Exception('The set name is missing on foreign key field '.$fkFieldName);
        }

        list($prefix, $appSetName) = $this->extractSetPrefixAndName($setName);

        $set = $this->App()->$appSetName();
        $set->setName($fkField->getName());
        $set->setDescription($fkField->getDescription());

        $this->aField[$fkFieldName] = $set;
        $set->setParentSet($this);
        return $this;
    }


    /**
     *
     * @param string $accessName
     * @param string $type
     */
    public function addAccessRight($accessName, $type = 'acl')
    {
        $this->accessRights[$accessName] = $type;
    }


    /**
     * @return array
     */
    public function getAccessRights()
    {
        return $this->accessRights;
    }



    /**
     * @return app_CustomField[]
     */
    public function getCustomFields()
    {
        $App = $this->App();

        if (null === $this->customFields) {
            $this->customFields = array();

            if (isset($App->CustomField)) {
                $customFieldSet = $App->CustomFieldSet();
                $object = mb_substr(get_class($this), mb_strlen($App->classPrefix), -mb_strlen('Set'));
                try {
                    $customFields = $customFieldSet->select($customFieldSet->object->is($object));

                    foreach ($customFields as $customfield) {
                        $this->customFields[] = $customfield;

                        /*@var $customfield app_CustomField */

                    }
                } catch (ORM_BackEndSelectException $e) {
                    // table does not exist, this error is thrown by the install program while creating the sets
                }
            }
        }

        return $this->customFields;
    }




    /**
     * @return app_CustomField[]
     */
    public function selectCustomFields()
    {
        $App = $this->App();

        $customFieldSet= $App->CustomFieldSet();
        $object = mb_substr(get_class($this), mb_strlen($App->classPrefix), -mb_strlen('Set'));

        $customFields = $customFieldSet->select($customFieldSet->object->is($object));

        return $customFields;
    }


    /**
     * @return self
     */
    public function addCustomFields()
    {
        $customFields = $this->selectCustomFields();
        foreach ($customFields as $customField) {
            /*@var $customField app_CustomField */
            $description = $customField->name;
            $ormField = $customField->getORMField()
                ->setDescription($description);
            if ($ormField instanceof ORM_FkField) {
                $this->hasOne($customField->fieldname, $ormField->getForeignSetName())
                    ->setDescription($description);
            } else {
                $this->addFields($ormField);
            }
        }

        return $this;
    }


    /**
     * Similar to ORM_RecordSet::get() method  but throws a app_NotFoundException if the
     * record is not found.
     *
     * @since 1.0.18
     *
     * @throws ORM_Exception
     * @throws app_NotFoundException
     *
     * @param ORM_Criteria|string $mixedParam    Criteria for selecting records
     *                                           or the value for selecting record
     * @param string              $sPropertyName The name of the property on which
     *                                           the value applies. If not
     *                                           specified or null, the set's
     *                                           primary key will be used.
     *
     * @return app_Record
     */
    public function request($mixedParam = null, $sPropertyName = null)
    {
        $record = $this->get($mixedParam, $sPropertyName);
        if (!isset($record)) {
            // This will remove the default criteria for TraceableRecords and
            // fetch even 'deleted' ones.
            $this->setDefaultCriteria(null);
            $record = $this->get($mixedParam, $sPropertyName);
            if (isset($record)) {
                throw new app_DeletedRecordException($record, $mixedParam);
            }
            throw new app_NotFoundException($this, $mixedParam);
        }
        return $record;
    }


    /**
     * Defines if records can be created by the current user.
     *
     * @return boolean
     */
    public function isCreatable()
    {
        return false;
    }


    /**
     * Returns a criterion matching records readable by the current user.
     *
     * @since 1.0.21
     *
     * @return ORM_Criterion
     */
    public function isReadable()
    {
        bab_debug('The query in '.get_class($this).' use the default isReadable() method: none()');
        return $this->none();
    }


    /**
     * Returns a criterion matching records updatable by the current user.
     *
     * @since 1.0.21
     *
     * @return ORM_Criterion
     */
    public function isUpdatable()
    {
        return $this->none();
    }

    /**
     * Returns a criterion matching records deletable by the current user.
     *
     * @since 1.0.21
     *
     * @return ORM_Criterion
     */
    public function isDeletable()
    {
        return $this->none();
    }
}



/**
 *
 *
 * @method app_RecordSet getParentSet()
 */
class app_Record extends ORM_Record implements app_Object_Interface
{
    /**
     * @var Func_App
     */
    protected $app = null;




    /**
     * Returns the value of the specified field or an iterator if $sFieldName represents a 'Many relation'.
     *
     * @param string $sFieldName The name of the field or relation for which the value must be returned.
     * @param array  $args       Optional arguments.
     *
     * @return mixed The value of the field or null if the field is not a part of the record.
     */
    public function __call($sFieldName, $args)
    {
        $value = $this->oParentSet->getBackend()->getRecordValue($this, $sFieldName);
        $field = $this->oParentSet->$sFieldName;
        if (!is_null($value) && $field instanceof ORM_FkField) {

            $sClassName = $field->getForeignSetName();

            $App = $this->App();
            $prefixLength = mb_strlen($App->classPrefix);
            $methodName = mb_substr($sClassName, $prefixLength);
            $set = $App->$methodName();

            $set->setName($field->getName());
            $set->setDescription($field->getDescription());

            $record = $set->get($value);
            return $record;
        }
        return $value;
    }

    /**
     * {@inheritDoc}
     * @see app_Object_Interface::setApp()
     */
    public function setApp(Func_App $app)
    {
        $this->app = $app;
        return $this;
    }

    /**
     * Get APP object to use with this record
     *
     * @return Func_App
     */
    public function App()
    {
        if (!isset($this->app)) {
            // If the app object was not specified (through the setApp() method),
            // we set it as parent set's App.
            $this->setApp($this->getParentSet()->App());
        }
        return $this->app;
    }


    /**
     * Returns the base class name of a record.
     * For example xxx_Contact will return 'Contact'.
     *
     * @since 1.0.40
     * @return string
     */
    public function getClassName()
    {
        list(, $classname) = explode('_', get_class($this));
        return $classname;
    }


    /**
     * Returns the string reference corresponding to the record.
     *
     * @return string 	A reference string (e.g. Contact:12)
     */
    public function getRef()
    {
        if (!isset($this->id)) {
            throw new app_Exception('Trying to get the reference string of a record without an id.');
        }
        $classname = $this->getClassName();
        return $classname . ':' . $this->id;
    }





    /**
     * Deletes the record with respect to referential integrity.
     *
     * Uses referential integrity as defined by hasManyRelation to delete/update
     * referenced elements.
     *
     * @see app_RecordSet::hasMany()
     *
     * @return self
     */
    public function delete()
    {
        $App = $this->App();

        $set = $this->getParentSet();
        $recordIdName = $set->getPrimaryKey();
        $recordId = $this->$recordIdName;

        // Uses referential integrity as defined by hasManyRelation to delete/update
        // referenced elements.
        $manyRelations = $set->getHasManyRelations();


        foreach ($manyRelations as $manyRelation) {
            /* @var $manyRelation ORM_ManyRelation */

            $foreignSetClassName = $manyRelation->getForeignSetClassName();
            $foreignSetFieldName = $manyRelation->getForeignFieldName();
            $method = mb_substr($foreignSetClassName, mb_strlen($App->classPrefix));
            $foreignSet = $App->$method();

            switch ($manyRelation->getOnDeleteMethod()) {

                case ORM_ManyRelation::ON_DELETE_SET_NULL:

                    $foreignRecords = $foreignSet->select($foreignSet->$foreignSetFieldName->is($recordId));

                    foreach ($foreignRecords as $foreignRecord) {
                        $foreignRecord->$foreignSetFieldName = 0;
                        $foreignRecord->save();
                    }

                    break;

                case ORM_ManyRelation::ON_DELETE_CASCADE:

                    $foreignRecords = $foreignSet->select($foreignSet->$foreignSetFieldName->is($recordId));

                    foreach ($foreignRecords as $foreignRecord) {
                        $foreignRecord->delete();
                    }

                    break;

                case ORM_ManyRelation::ON_DELETE_NO_ACTION:
                default:
                    break;

            }
        }


        // We remove all links to and from this record.
        $linkSet = $App->LinkSet();

        $linkSet->delete(
            $linkSet->sourceClass->is(get_class($this))->_AND_($linkSet->sourceId->is($recordId))
            ->_OR_(
                $linkSet->targetClass->is(get_class($this))->_AND_($linkSet->targetId->is($recordId))
            )
        );


        $set->delete($set->$recordIdName->is($recordId));

        return $this;
    }



    /**
     * Reassociates all data asociated to the record to another
     * specified one.
     *
     * @param	int		$id
     *
     * @return self
     */
    public function replaceWith($id)
    {
        $App = $this->App();

        $set = $this->getParentSet();
        $recordIdName = $set->getPrimaryKey();
        $recordId = $this->$recordIdName;

        // Use referential integrity as defined by hasManyRelation to delete/update
        // referenced elements.
        $manyRelations = $set->getHasManyRelations();


        foreach ($manyRelations as $manyRelation) {
            /* @var $manyRelation ORM_ManyRelation */

            $foreignSetClassName = $manyRelation->getForeignSetClassName();
            $foreignSetFieldName = $manyRelation->getForeignFieldName();
            $method = mb_substr($foreignSetClassName, mb_strlen($App->classPrefix));
            $foreignSet = $App->$method();
            // $foreignSet = new $foreignSetClassName($App);
            $foreignRecords = $foreignSet->select($foreignSet->$foreignSetFieldName->is($recordId));

            foreach ($foreignRecords as $foreignRecord) {
                $foreignRecord->$foreignSetFieldName = $id;
                $foreignRecord->save();
            }
        }


        // We replace all links to and from this record.
        $linkSet = $App->LinkSet();

        $links = $linkSet->select(
            $linkSet->sourceClass->is(get_class($this))->_AND_($linkSet->sourceId->is($recordId))
        );

        foreach ($links as $link) {
            $link->sourceId = $id;
            $link->save();
        }

        $links = $linkSet->select(
            $linkSet->targetClass->is(get_class($this))->_AND_($linkSet->targetId->is($recordId))
        );

        foreach ($links as $link) {
            $link->targetId = $id;
            $link->save();
        }

        return $this;
    }


    /**
     *
     *
     * @return array
     */
    public function getRelatedRecords()
    {
        $App = $this->App();

        $set = $this->getParentSet();
        $recordIdName = $set->getPrimaryKey();
        $recordId = $this->$recordIdName;

        // Use referential integrity as defined by hasManyRelation to delete/update
        // referenced elements.
        $manyRelations = $set->getHasManyRelations();

        $relatedRecords = array();

        foreach ($manyRelations as $manyRelation) {
            /* @var $manyRelation ORM_ManyRelation */

            $foreignSetClassName = $manyRelation->getForeignSetClassName();
            $foreignSetFieldName = $manyRelation->getForeignFieldName();

            $method = mb_substr($foreignSetClassName, mb_strlen($App->classPrefix));
            $foreignSet = $App->$method();
            // $foreignSet = new $foreignSetClassName($App);
            $foreignRecords = $foreignSet->select($foreignSet->$foreignSetFieldName->is($recordId));


            if ($foreignRecords->count() > 0) {
                $relatedRecords[$foreignSetClassName] = $foreignRecords;
            }
        }

        return $relatedRecords;
    }






    /**
     * Upload path for record attachments
     *
     * @return bab_Path
     */
    public function uploadPath()
    {
        $path = $this->App()->uploadPath();

        if (null === $path)
        {
            throw new Exception('Missing upload path information');
            return null;
        }

        $path->push(get_class($this));
        $path->push($this->id);

        return $path;
    }






    /**
     * import a value into a traceable record property if the value is not equal
     *
     * @param	string	$name		property name
     * @param	mixed	$value		value to set
     *
     * @return int		1 : the value has been modified | 0 : no change
     */
    protected function importProperty($name, $value)
    {
        if (((string) $this->$name) !== ((string) $value)) {
            $this->$name = $value;
            return 1;
        }

        return 0;
    }



    /**
     * import a value into a tracable record property if the value is not equal, try with multiple date format
     * this method work for date field 0000-00-00
     *
     * @param	string	$name		property name
     * @param	mixed	$value		value to set
     *
     * @return int		1 : the value has been modified | 0 : no change
     */
    protected function importDate($name, $value)
    {
        if (preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/',$value)) {
            return $this->importProperty($name, $value);
        }

        // try in DD/MM/YYYY format

        if (preg_match('/(?P<day>[0-9]+)\/(?P<month>[0-9]+)\/(?P<year>[0-9]{2,4})/',$value, $matches)) {

            $value = sprintf('%04d-%02d-%02d', (int) $matches['year'], (int) $matches['month'], (int) $matches['day']);

            return $this->importProperty($name, $value);
        }

    }




    /**
     *
     * @return string[]
     */
    public function getViews()
    {
        $App = $this->App();

        $customSectionSet = $App->CustomSectionSet();
        $customSections = $customSectionSet->select($customSectionSet->object->is($this->getClassName()));
        $customSections->groupBy($customSectionSet->view);

        $views = array();
        foreach ($customSections as $customSection) {
            $views[] = $customSection->view;
        }

        return $views;
    }



    /**
     * Checks if the record is readable by the current user.
     * @since 1.0.21
     * @return bool
     */
    public function isReadable()
    {
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->_AND_($set->id->is($this->id)))->count() == 1;
    }


    /**
     * Checks if the record is updatable by the current user.
     * @since 1.0.21
     * @return bool
     */
    public function isUpdatable()
    {
        $set = $this->getParentSet();
        return $set->select($set->isUpdatable()->_AND_($set->id->is($this->id)))->count() == 1;
    }

    /**
     * Checks if the record is deletable by the current user.
     * @since 1.0.21
     * @return bool
     */
    public function isDeletable()
    {
        $set = $this->getParentSet();
        return $set->select($set->isDeletable()->_AND_($set->id->is($this->id)))->count() == 1;
    }


    /**
     * Ensures that the record is readable by the current user or throws an exception.
     * @since 1.0.40
     * @param string $message
     * @throws app_AccessException
     */
    public function requireReadable($message = null)
    {
        if (!$this->isReadable()) {
            $App = $this->App();
            if (!isset($message)) {
                $message = $App->translate('Access denied');
            }
            throw new app_AccessException($message);
        }
    }

    /**
     * Ensures that the record is updatable by the current user or throws an exception.
     * @since 1.0.40
     * @param string $message
     * @throws app_AccessException
     */
    public function requireUpdatable($message = null)
    {
        if (!$this->isUpdatable()) {
            $App = $this->App();
            if (!isset($message)) {
                $message = $App->translate('Access denied');
            }
            throw new app_AccessException($message);
        }
    }

    /**
     * Ensures that the record is deletable by the current user or throws an exception.
     * @since 1.0.40
     * @param string $message
     * @throws app_AccessException
     */
    public function requireDeletable($message = null)
    {
        if (!$this->isDeletable()) {
            $App = $this->App();
            if (!isset($message)) {
                $message = $App->translate('Access denied');
            }
            throw new app_AccessException($message);
        }
    }
}

