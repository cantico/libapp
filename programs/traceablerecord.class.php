<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */

$App = app_App();
$App->includeRecordSet();


/**
 *
 * @property ORM_PkField		$id
 * @property ORM_IntField		$createdBy
 * @property ORM_DateTimeField	$createdOn
 * @property ORM_IntField		$modifiedBy
 * @property ORM_DateTimeField	$modifiedOn
 * @property ORM_IntField		$deletedBy
 * @property ORM_DateTimeField	$deletedOn
 * @property ORM_BoolField		$deleted
 */
class app_TraceableRecordSet extends app_RecordSet
{
    /**
     * @var bool
     */
    private $loggable = false;

    /**
     * @var bool
     */
    private $traceable = true;

    /**
     * @param Func_App $App
     */
    public function __construct(Func_App $App = null)
    {
        parent::__construct($App);

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_UserField('createdBy')
                    ->setDescription('Created by'),
            ORM_DateTimeField('createdOn')
                    ->setDescription('Created on'),
            ORM_UserField('modifiedBy')
                    ->setDescription('Modified by'),
            ORM_DateTimeField('modifiedOn')
                    ->setDescription('Modified on'),
            ORM_IntField('deletedBy')
                    ->setDescription('Deleted by'),
            ORM_DateTimeField('deletedOn')
                    ->setDescription('Deleted on'),
            ORM_BoolField('deleted')
                    ->setDescription('Deleted'),
            ORM_StringField('uuid')
                    ->setDescription('Universally Unique IDentifier')

        );

        // This condition will be applied whenever we select or join Records from this RecordSet.
        $this->setDefaultCriteria($this->deleted->is(false));
    }


    /**
     * Defines if the insertions/updates/deletions on the recordSet will be logged.
     *
     * @param bool $loggable
     * @return self
     */
    protected function setLoggable($loggable)
    {
        $this->loggable = $loggable;
        return $this;
    }


    /**
     * Checks if the insertions/updates/deletions on the recordSet will be logged.
     *
     * @return bool
     */
    protected function isLoggable()
    {
        return $this->loggable;
    }




    /**
     * Defines if the insertions/updates/deletions on the recordSet will be traced.
     *
     * @param bool $traceable
     * @return self
     */
    public function setTraceable($traceable)
    {
        $this->traceable = $traceable;
        return $this;
    }


    /**
     * Checks if the insertions/updates/deletions on the recordSet will be traced.
     *
     * @return bool
     */
    public function isTraceable()
    {
        return $this->traceable;
    }


    /**
     * @param app_TraceableRecord   $record
     * @param bool                  $noTrace
     */
    protected function logSave(app_TraceableRecord $record, $noTrace)
    {
        if (!$this->isLoggable()) {
            return;
        }
        $App = $this->App();
        $userId = bab_getUserId();
        $logSet = $App->LogSet();
        $log = $logSet->newRecord();
        $log->noTrace = $noTrace;
        $log->objectClass = get_class($record);
        $log->objectId = $record->id;
        $now = date('Y-m-d H:i:s');
        $log->modifiedOn = $now;
        $log->modifiedBy = $userId;
        $log->data = $logSet->serialize($record);
        $log->save();
    }


    /**
     * @param ORM_Criteria          $criteria
     * @param bool                  $noTrace
     */
    protected function logDelete(ORM_Criteria $criteria, $noTrace)
    {
        if (!$this->isLoggable()) {
            return;
        }
        $App = $this->App();
        $userId = bab_getUserId();
        $logSet = $App->LogSet();
        $deletedRecords = $this->select($criteria);
        foreach ($deletedRecords as $record) {
            $log = $logSet->newRecord();
            $log->noTrace = $noTrace;
            $log->objectClass = get_class($record);
            $log->objectId = $record->id;
            $now = date('Y-m-d H:i:s');
            $log->modifiedOn = $now;
            $log->modifiedBy = $userId;
            $log->data = '';
            $log->save();
        }
    }

    /**
     * Returns an iterator on records matching the specified criteria.
     * The iterator will not include records flagged as deleted unless
     * the $includeDeleted parameter is set to true.
     *
     * @param ORM_Criteria	$criteria			Criteria for selecting records.
     * @param bool			$includeDeleted		True to include delete-flagged records.
     *
     * @return ORM_Iterator				Iterator on success, null if the backend has not been set
     */
    public function select(ORM_Criteria $criteria = null, $includeDeleted = false)
    {
        if ($includeDeleted) {
            $this->setDefaultCriteria(null);
        }
        return parent::select($criteria);
    }


    /**
     * Returns the first item matching the specified criteria.
     * The item will not include records flagged as deleted unless
     * the $includeDeleted parameter is set to true.
     *
     * @param ORM_Criteria	$criteria			Criteria for selecting records.
     * @param string		$sPropertyName		The name of the property on which the value applies. If not specified or null, the set's primary key will be used.
     * @param bool			$includeDeleted		True to include delete-flagged records.
     *
     * @return ORM_Item				Iterator on success, null if the backend has not been set
     */
    /*public function get(ORM_Criteria $criteria = null, $sPropertyName = null, $includeDeleted = false)
    {
        if ($includeDeleted) {
            $this->setDefaultCriteria(null);
        }
        return parent::get($criteria, $sPropertyName);
    }*/


    /**
     * Deleted records matching the specified criteria.
     * If $definitive is false, records are not actually deleted
     * from the database but only flagged as so.
     *
     * @param ORM_Criteria	$criteria		The criteria for selecting the records to delete.
     * @param bool			$definitive		True to delete permanently the record.
     *
     * @return boolean				True on success, false otherwise
     */
    public function delete(ORM_Criteria $criteria = null, $definitive = false)
    {
        $definitive = $definitive || !$this->isTraceable();
        $this->logDelete($criteria, $definitive);
        if ($definitive) {
            return parent::delete($criteria);
        }

        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        $now = BAB_DateTime::now()->getIsoDateTime();

        $records = $this->select($criteria);


        foreach ($records as $record) {
            /* @var $record app_TraceableRecord */
            // Could be optimized at ORM level
            $record->deleted = true;
            $record->deletedOn = $now;
            $record->deletedBy = bab_getUserId();
            if (!parent::save($record)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Saves a record and keeps traces of the user doing it.
     *
     * @param app_TraceableRecord	$record			The record to save.
     * @param bool					$noTrace		True to bypass the tracing of modifications.
     *
     * @return boolean				True on success, false otherwise
     */
    public function save(ORM_Record $record, $noTrace = false)
    {
        $noTrace = $noTrace || !$this->isTraceable();
        $this->logSave($record, $noTrace);
        if ($noTrace) {
            return parent::save($record);
        }

        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

        $now = BAB_DateTime::now()->getIsoDateTime();

        // We first check if the record already has a createdBy.
        $set = $record->getParentSet();
        $primaryKey = $set->getPrimaryKey();

        if (empty($record->{$primaryKey})) {
            $record->initValue('createdBy', bab_getUserId());
            $record->initValue('createdOn', $now);
            $record->initValue('uuid', $this->uuid());
        }

        $record->initValue('modifiedBy', bab_getUserId());
        $record->initValue('modifiedOn', $now);

        return parent::save($record);
    }




    /**
     * Generates a Universally Unique IDentifier, version 4.
     * RFC 4122 (http://www.ietf.org/rfc/rfc4122.txt)
     * @return string
     */
    private function uuid()
    {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ) );
    }

    /**
     * Get record by UUID or null if the record does not exists or is deleted or if the uuid is empty
     * @param	string	$uuid
     * @return app_TraceableRecord
     */
    public function getRecordByUuid($uuid)
    {
        if ('' === (string) $uuid) {
            return null;
        }


        $record = $this->get($this->uuid->is($uuid));

        if (!isset($record)) {
            return null;
        }

        if (!($record instanceOf app_TraceableRecord)) {
            return null;
        }

        if ($record->deleted) {
            return null;
        }

        return $record;
    }







    /**
     * Returns an iterator of traceableRecord linked to the specified source,
     * optionally filtered on the specified link type.
     *
     * @param	app_Record | array		$source			source can be an array of record
     * @param	string					$linkType
     *
     * @return ORM_Iterator
     */
    public function selectLinkedTo($source, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();
        if (is_array($source) || ($source instanceof Iterator)) {
            return $linkSet->selectForSources($source, $this->getRecordClassName(), $linkType);
        } else {
            return $linkSet->selectForSource($source, $this->getRecordClassName(), $linkType);
        }
    }

    /**
     * Returns an iterator of traceableRecord linked to the specified target,
     * optionally filtered on the specified link type.
     *
     * @param	app_Record | array		$target			target can be an array of record
     * @param	string					$linkType
     *
     * @return ORM_Iterator
     */
    public function selectLinkedFrom($target, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();
        if (is_array($target) || ($target instanceof Iterator)) {
            return $linkSet->selectForTargets($target, $this->getRecordClassName(), $linkType);
        } else {
            return $linkSet->selectForTarget($target, $this->getRecordClassName(), $linkType);
        }
    }


    /**
     * Returns a criteria usable to select records of this set which are source of app_Links to the specified app_Record.
     *
     * @param app_Record $target
     * @param string     $linkType
     * @return ORM_Criteria
     */
    public function isSourceOf(app_Record $target, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();

        $linkSet->hasOne('sourceId', get_class($this));

        $criteria =	$linkSet->targetClass->is(get_class($target))
            ->_AND_($linkSet->targetId->is($target->id))
            ->_AND_($linkSet->sourceClass->is($this->getRecordClassName()));
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $criteria = $this->id->in($criteria);

        return $criteria;
    }



    /**
     * Returns a criteria usable to select records of this set which are target of app_Links to the specified app_Record.
     *
     * @param app_Record $source
     * @param string     $linkType
     * @return ORM_Criteria
     */
    public function isTargetOf(app_Record $source, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();

        $linkSet->hasOne('targetId', get_class($this));

        $criteria =	$linkSet->sourceClass->is(get_class($source))
            ->_AND_($linkSet->sourceId->is($source->id))
            ->_AND_($linkSet->targetClass->is($this->getRecordClassName()));
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $criteria = $this->id->in($criteria);

        return $criteria;
    }


    /**
     * Returns a criteria usable to select records of this set which are target of app_Links from the specified app_Records.
     *
     * @since 1.0.23
     *
     * @param app_Record[]     $sources
     * @param string|null      $linkType
     * @return ORM_Criteria
     */
    public function isTargetOfAny($sources, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();
        $linkSet->hasOne('targetId', get_class($this));

        $sourceIdsByClasses = array();
        foreach ($sources as $source) {
            $sourceClass = get_class($source);
            if (!isset($sourceIdsByClasses[$sourceClass])) {
                $sourceIdsByClasses[$sourceClass] = array();
            }
            $sourceIdsByClasses[$sourceClass][] = $source->id;
        }

        $sourcesCriteria = array();
        foreach ($sourceIdsByClasses as $sourceClass => $sourceIds) {
            $sourcesCriteria[] = $linkSet->sourceClass->is($sourceClass)->_AND_($linkSet->sourceId->in($sourceIds));
        }

        $criteria =	$linkSet->all(
            $linkSet->targetClass->is($this->getRecordClassName()),
            $linkSet->any($sourcesCriteria)
        );
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $criteria = $this->id->in($criteria);

        return $criteria;
    }


    /**
     * Returns a criteria usable to select records of this set which are source of app_Links to the specified app_Records.
     *
     * @since 1.0.23
     *
     * @param app_Record[]     $targets
     * @param string|null      $linkType
     * @return ORM_Criteria
     */
    public function isSourceOfAny($targets, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();
        $linkSet->hasOne('sourceId', get_class($this));

        $targetIdsByClasses = array();
        foreach ($targets as $target) {
            $targetClass = get_class($target);
            if (!isset($targetIdsByClasses[$targetClass])) {
                $targetIdsByClasses[$targetClass] = array();
            }
            $targetIdsByClasses[$targetClass][] = $target->id;
        }

        $targetsCriteria = array();
        foreach ($targetIdsByClasses as $targetClass => $targetIds) {
            $targetsCriteria[] = $linkSet->targetClass->is($targetClass)->_AND_($linkSet->targetId->in($targetIds));
        }

        $criteria =	$linkSet->all(
            $linkSet->sourceClass->is($this->getRecordClassName()),
            $linkSet->any($targetsCriteria)
        );
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $criteria = $this->id->in($criteria);

        return $criteria;
    }
}


/**
 * A traceable record automatically stores by whom and when it was created,
 * modified and even deleted.
 *
 * By default "deleted" records (through the standard delete() methods)
 * actually stay in the database and are only flagged as deleted.
 *
 * @property int        $id
 * @property int		$createdBy
 * @property string		$createdOn
 * @property int		$modifiedBy
 * @property string		$modifiedOn
 * @property int		$deletedBy
 * @property string		$deletedOn
 * @property string		$uuid
 * @property bool		$deleted
 */
class app_TraceableRecord extends app_Record
{
    /**
     * Saves the record.
     *
     * @param bool		$noTrace		True to bypass the tracing of modifications.
     *
     * @return boolean True on success, false otherwise
     */
    public function save($noTrace = false)
    {
        return $this->getParentSet()->save($this, $noTrace);
    }



    /**
     * @return bool
     */
    public function isLinkedTo(app_Record $source, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();

        $criteria =	$linkSet->sourceClass->is(get_class($source))
            ->_AND_($linkSet->sourceId->is($source->id))
            ->_AND_($linkSet->targetClass->is(get_class($this)))
            ->_AND_($linkSet->targetId->is($this->id));
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $links = $linkSet->select($criteria);

        $isLinked = ($links->count() > 0);

        $linkSet->__destruct();
        unset($linkSet);

        return $isLinked;
    }



    /**
     * @return bool
     */
    public function isSourceOf(app_Record $target, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();

        $criteria =	$linkSet->targetClass->is(get_class($target))
            ->_AND_($linkSet->targetId->is($target->id))
            ->_AND_($linkSet->sourceClass->is(get_class($this)))
            ->_AND_($linkSet->sourceId->is($this->id));
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $links = $linkSet->select($criteria);

        $isLinked = ($links->count() > 0);

        $linkSet->__destruct();
        unset($linkSet);

        return $isLinked;
    }



    /**
     * Link record to $source
     *
     * @return app_TraceableRecord
     */
    public function linkTo(app_Record $source, $linkType = '')
    {
        $linkSet = $this->App()->LinkSet();
        /* @var $link app_Link */
        $link = $linkSet->newRecord();
        $link->sourceClass = get_class($source);
        $link->sourceId = $source->id;
        $link->targetClass = get_class($this);
        $link->targetId = $this->id;
        $link->type = $linkType;
        $link->save();

        $link->__destruct();
        unset($link);

        $linkSet->__destruct();
        unset($linkSet);

        return $this;
    }
}
