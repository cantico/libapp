��    1      �  C   ,      8     9     G     X     ^     e     k     t     �     �     �     �     �     �     �     �     �     �     �          +     2     :  .   I     x  	   {     �     �     �     �     �     �     �     �     �  	   �  '     '   .     V  Z   q  <   �  .   	     8     P     i     }  $   �  *   �     �  �  �     �	     �	     �	     �	     �	     �	     

     %
     *
     8
  	   H
     R
     d
     u
     �
     �
     �
     �
  .   �
        
          4         U     X     g  #   j     �  6   �     �     �  
   �     �          )  ,   D  *   q     �  T   �  Q     5   c     �     �     �     �  %   �  7        Q     $   %          (                                 *                !       1       	             /                          -   .   "   '      &      #       ,                 +      )                                   
   0              Access denied Advanced filters Bytes Cancel Cards Checkbox Confirm delete? Date Date and time Decimal number Delete Deleted by: %s Deleted on: %s Deletion Detailed list Email address Export list Exported columns Failed to write file to disk. Filter History Integer number It will not be possible to undo this deletion. KB Line edit MB Missing a temporary folder. No No file was uploaded. Products database Save Search Select all / none Selection list Text edit The field %s should be a decimal number The field %s should be a integer number The record does not exists The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form. The uploaded file exceeds the upload_max_filesize directive. The uploaded file was only partially uploaded. This %s does not exists This %s has been deleted Unknown File Error. Yes You do not have access to this page. You must be logged in to access this page. object Project-Id-Version: 
POT-Creation-Date: 2018-11-19 14:30+0100
PO-Revision-Date: 2019-02-06 12:05+0100
Last-Translator: Laurent Choulette <laurent.choulette@cantico.fr>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: ../programs
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: app_translate;app_translate:1,2;translate:1,2;translate
X-Poedit-SearchPath-0: .
 Accès refusé Filtres avancés Octets Annuler Cartes Case à cocher Confirmer la suppression ? Date Date et heure Nombre décimal Supprimer Supprimé par: %s Supprimé le: %s Suppression Liste détaillée Adresse de messagerie Exporter la liste Exporter les colonnes Impossible d'écrire le fichier sur le disque. Filtre Historique Nombre entier Il ne sera pas possible d'annuler cette suppression. Ko Ligne de texte Mo Le fichier temporaire est manquant. Non Aucun fichier n'a été téléchargé vers le serveur. Base de donnés des produits Enregistrer Rechercher Sélectionner tout / rien Liste déroulante Texte sur plusieurs lignes Le champ %s devrait être un nombre décimal Le champ %s devrait être un nombre entier Cet enregistrement n'existe pas Le fichier à télécharger excède la taille maximale autorisée par le formulaire. Le fichier à télécharger excède la taille maximale autorisée par le serveur. Le fichier n'a été que partiellement téléchargé. Cet objet %s n'existe pas Cet objet %s à été supprimé Erreur de fichier inconnue. Oui Vous n'avez pas accès à cette page. Vous devez être connecté pour accéder à cette page. objet 